//
//  WeatherObservation.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-07-17.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit
import GoogleMaps

class WeatherObservation: NSObject {
    
    var stationName = "--"
    var stationTag = "--"
    var stationId = "--"
    var latitude = 0.0
    var longitude = 0.0 { // longitude -180 to 180
        didSet {
            if longitude >= 180 {
                longitude -= 360
            }
        }
    }
    var elevation = "--"
    var observationTime = "--"
    var temperature = "--"
    var tempHi = "--"
    var tempLo = "--"
    var humidity = "--"
    var dewPoint = "--"
    var pressure = "--"
    var pressureTrend = "--"
    var insolation = "--"
    var uvIndex = "--"
    var rain = "--"
    var rainRate = "--"
    var windSpeed = "--"
    var windSpeedDirection = "--"
    var windHeading = "--"
    var windSpeedMax = "--"
    var evapotranspiration = "--"
    var insolationPredicted = "--"
    
}

extension WeatherObservation: GMUClusterItem {
    var position: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
    }
    /*
    var name: String? {
        get {
            return stationName
        }
    }
 */
    var tempData: String {
        get {
            return temperature + "°"
        }
    }
    var humidityData: String {
        get {
            return humidity + "%"
        }
    }
    var clusterId: String {
        get {
            return stationId
        }
    }
}

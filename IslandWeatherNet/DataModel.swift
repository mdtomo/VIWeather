//
//  DataModel.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-08-31.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import Foundation
import RealmSwift

enum DataModelType {
    case settings
    case stations
}



class DataModel: Object {
    //MARK: Settings
    
    static func load(_ dataType: DataModelType) -> NSObject? {
        var settings: Results<Settings>!
        do {
            let realm = try Realm()
            if dataType == .settings {
                settings = realm.objects(Settings.self)
                if settings.isEmpty {
                    let defaultSettings = Settings()
                    try realm.write {
                        realm.add(defaultSettings)
                    }
                    settings = realm.objects(Settings.self)
                    print(settings)
                    return settings
                } else {
                    return settings
                }
            } else if dataType == .stations {
                return realm.objects(WeatherStation.self)
            }
            
        } catch {
            print("Error loading data!" + " \(error)")
        }
        return nil
    }
    
    static func save(_ dataType: DataModelType, data: NSObject) {
        do {
            let realm = try Realm()
            try realm.write {
                if dataType == .settings {
                    let settings = data as! Settings
                    realm.add(settings, update: true)
                } else if dataType == .stations {
                    let stations = data as! [WeatherStation]
                    realm.add(stations, update: true)
                }
            }
        } catch {
            print("Error saving! " + "\(error)")
        }
    }
}

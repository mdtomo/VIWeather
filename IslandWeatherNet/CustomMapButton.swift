//
//  CustomMapButton.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-11-27.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import Foundation

class CustomMapButton: NSObject {
    
    static func location(enabled: Bool) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 44, height: 44), false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        let rectanglePath = UIBezierPath()
        rectanglePath.move(to: CGPoint(x: 22, y: 22))
        rectanglePath.addLine(to: CGPoint(x: 22, y: 38))
        rectanglePath.addLine(to: CGPoint(x: 38, y: 6))
        rectanglePath.addLine(to: CGPoint(x: 6, y: 22))
        rectanglePath.addLine(to: CGPoint(x: 22, y: 22))
        rectanglePath.close()
        if enabled {
            teal.setFill()
        } else {
            UIColor.white.setFill()
        }
        rectanglePath.fill()
        context!.saveGState()
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    static func clear() -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 44, height: 44), false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        //// Color Declarations
        let red = UIColor(red: 0.884, green: 0.162, blue: 0.008, alpha: 1.000)
        
        //// Arrow Drawing
        let arrowPath = UIBezierPath()
        arrowPath.move(to: CGPoint(x: 23, y: 6.5))
        arrowPath.addCurve(to: CGPoint(x: 33.83, y: 20.75), controlPoint1: CGPoint(x: 23, y: 6.5), controlPoint2: CGPoint(x: 33.83, y: 20.75))
        arrowPath.addLine(to: CGPoint(x: 28, y: 20.75))
        arrowPath.addCurve(to: CGPoint(x: 28, y: 36), controlPoint1: CGPoint(x: 28, y: 25.79), controlPoint2: CGPoint(x: 28, y: 36))
        arrowPath.addLine(to: CGPoint(x: 18, y: 36))
        arrowPath.addCurve(to: CGPoint(x: 18, y: 20.75), controlPoint1: CGPoint(x: 18, y: 36), controlPoint2: CGPoint(x: 18, y: 25.79))
        arrowPath.addLine(to: CGPoint(x: 12.18, y: 20.75))
        arrowPath.addCurve(to: CGPoint(x: 23, y: 6.5), controlPoint1: CGPoint(x: 12.17, y: 20.75), controlPoint2: CGPoint(x: 23, y: 6.5))
        arrowPath.addLine(to: CGPoint(x: 23, y: 6.5))
        arrowPath.close()
        UIColor.white.setFill()
        arrowPath.fill()
        
        //// line Drawing
        let linePath = UIBezierPath()
        linePath.move(to: CGPoint(x: 7.5, y: 5.5))
        linePath.addLine(to: CGPoint(x: 37.5, y: 38.5))
        linePath.lineCapStyle = .round;
        
        red.setStroke()
        linePath.lineWidth = 4
        linePath.stroke()
        
        
        //// line2 Drawing
        context!.saveGState()
        context!.translateBy(x: 37.74, y: 5.08)
        context!.rotate(by: 82.75 * CGFloat(M_PI) / 180)
        
        let line2Path = UIBezierPath()
        line2Path.move(to: CGPoint(x: 0, y: 0))
        line2Path.addLine(to: CGPoint(x: 30, y: 33))
        line2Path.lineCapStyle = .round;
        
        red.setStroke()
        line2Path.lineWidth = 4
        line2Path.stroke()
        context!.restoreGState()
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    static func cross() -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 32, height: 32), false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        let xPath = UIBezierPath()
        xPath.move(to: CGPoint(x: 8, y: 8))
        xPath.addLine(to: CGPoint(x: 24, y: 24))
        xPath.move(to: CGPoint(x: 8, y: 24))
        xPath.addLine(to: CGPoint(x: 24, y: 8))
        xPath.lineCapStyle = .round
        UIColor.white.setStroke()
        xPath.lineWidth = 4
        xPath.stroke()
        context!.saveGState()
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    static func plus() -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 32, height: 32), false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        context!.saveGState()
        context!.translateBy(x: 16, y: 16)
        context!.rotate(by: -45 * CGFloat(M_PI) / 180)
        
        let plusPath = UIBezierPath()
        plusPath.move(to: CGPoint(x: -8, y: -8))
        plusPath.addLine(to: CGPoint(x: 8, y: 8))
        plusPath.move(to: CGPoint(x: -8, y: 8))
        plusPath.addLine(to: CGPoint(x: 8, y: -8))
        plusPath.lineCapStyle = .round
        UIColor.white.setStroke()
        plusPath.lineWidth = 4
        plusPath.stroke()
        
        context!.restoreGState()
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    static func dash() -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 32, height: 32), false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        let dashPath = UIBezierPath()
        dashPath.move(to: CGPoint(x: 8, y: 16))
        dashPath.addLine(to: CGPoint(x: 24, y: 16))
        dashPath.lineCapStyle = .round;
        
        UIColor.white.setStroke()
        dashPath.lineWidth = 4
        dashPath.stroke()
        
        context!.saveGState()
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    static func more() -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 32, height: 32), false, 0)
        let context = UIGraphicsGetCurrentContext()
        
        //// Color Declarations
        let color2 = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1.000)
        
        //// triangle Drawing
        context!.saveGState()
        context!.translateBy(x: 21, y: 11)
        context!.rotate(by: 45 * CGFloat(M_PI) / 180)
        
        let trianglePath = UIBezierPath()
        trianglePath.move(to: CGPoint(x: 0, y: -7))
        trianglePath.addLine(to: CGPoint(x: 6.06, y: 3.5))
        trianglePath.addLine(to: CGPoint(x: -6.06, y: 3.5))
        trianglePath.close()
        color2.setFill()
        trianglePath.fill()
        
        context!.restoreGState()
        
        //// triangle2 Drawing
        context!.saveGState()
        context!.translateBy(x: 11, y: 21)
        context!.rotate(by: -15.02 * CGFloat(M_PI) / 180)
        
        let triangle2Path = UIBezierPath()
        triangle2Path.move(to: CGPoint(x: 0, y: -7))
        triangle2Path.addLine(to: CGPoint(x: 6.06, y: 3.5))
        triangle2Path.addLine(to: CGPoint(x: -6.06, y: 3.5))
        triangle2Path.close()
        color2.setFill()
        triangle2Path.fill()
        
        context!.restoreGState()
        
        context!.saveGState()
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    // Recenter map on island icon button
}

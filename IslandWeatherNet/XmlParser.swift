//
//  XmlParser.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-08-29.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import Foundation

protocol XmlParserDelegate: class {
    func xmlParserDidParseData(_ requestType: RequestDataType, data: NSObject)
    func xmlParserDidFailWithError(_ requestType: RequestDataType, error: NSError)
}

class XmlParser: NSObject, XMLParserDelegate, RequestFromServerParserDelegate {
    
    var parser = XMLParser()
    var currentXmlElement = ""
    var observation = WeatherObservation()
    var allObservations = [WeatherObservation]()
    var allStations = [WeatherStation]()
    
    var isAllObservations = false
    
    weak var delegate: XmlParserDelegate?
    
    func parse(_ requestType: RequestDataType, data: Data) {
        parser = XMLParser.init(data: data)
        parser.delegate = self
        if parser.parse() {
                if requestType == .allObservations  { // Save the list of WeatherStations
                    delegate?.xmlParserDidParseData(requestType, data: allObservations as NSObject)
                } else if requestType == .allStations {
                    delegate?.xmlParserDidParseData(requestType, data: allStations as NSObject)
                } else if requestType == .observation {
                    delegate?.xmlParserDidParseData(requestType, data: observation as NSObject)
                }
            
        } else {
            let error = NSError(domain: "Parse Error", code: 0, userInfo: nil)
            delegate?.xmlParserDidFailWithError(requestType, error: error)
        }
    
    }
    
    func requestDidReceiveRequestData(_ requestType: RequestDataType, data: Data) {
        parse(requestType, data: data)
    }
    
//    func parser(_ parser: XMLParser, foundAttributeDeclarationWithName attributeName: String, forElement elementName: String, type: String?, defaultValue: String?) {
//        print(attributeName + "FOR" + elementName)
//    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        currentXmlElement = elementName
        if currentXmlElement == "marker" { // Process the list of stations from stations.xml and add to array of WeatherStation objects.
            let station = WeatherStation()
            for (key, value) in attributeDict {
                switch key {
                case "name":
                    station.name = value
                case "id":
                    station.id = Int(value) ?? 0
                case "lat":
                    station.latitude = Double(value) ?? 0.0
                case "long":
                    station.longitude = Double(value) ?? 0.0
                case "elev":
                    station.elevation = Int(value) ?? 0
                case "tag":
                    station.tag = value
                default:
                    break
                    
                }
            }
            allStations.append(station)
        } else if currentXmlElement == "current_conditions" {
            isAllObservations = true
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        // Observation
        switch currentXmlElement {
            case "station_long_name":
                observation.stationName = string
            case "station_name":
                observation.stationTag = string
            case "station_id":
                observation.stationId = string
            case "latitude":
                observation.latitude =  Double(string) ?? 0.0
            case "longitude":
                observation.longitude = Double(string) ?? 0.0
            case "elevation":
                observation.elevation = string
            case "observation_time":
                observation.observationTime = string
            case "temperature":
                observation.temperature = string
            case "temperature_low":
                observation.tempLo = string
            case "temperature_high":
                observation.tempHi = string
            case "humidity":
                observation.humidity = string
            case "dewpoint":
                observation.dewPoint = string
            case "pressure":
                observation.pressure = string
            case "pressure_trend":
                observation.pressureTrend = string
            case "insolation":
                observation.insolation = string
            case "uv_index":
                observation.uvIndex = string
            case "rain":
                observation.rain = string
            case "rain_rate":
                observation.rainRate = string
            case "wind_speed":
                observation.windSpeed = string
            case "wind_speed_direction":
                observation.windSpeedDirection = string
            case "wind_speed_max":
                observation.windSpeedMax = string
            case "wind_speed_heading":
                observation.windHeading = string
            case "evapotranspiration":
                observation.evapotranspiration = string
            case "insolation_predicted":
                observation.insolationPredicted = string
//            case "error_message":
//                faultyStation()
//            case "station_fault":
//                faultyStation()
            default:
                break
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if isAllObservations && elementName == "current_observation" {
            allObservations.append(observation)
            observation = WeatherObservation()
        }
        if isAllObservations && elementName == "current_conditions" {
            isAllObservations = false
        }
        currentXmlElement = ""
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("\(parseError)")
    }
    
    

}

//
//  AboutViewController.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-12-25.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit
import WebKit

class AboutViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {

    @IBOutlet weak var webViewContainer: UIView!
    var webView: WKWebView!
    var urlString = "https://viweather.flirky.com/about"
    //let progressLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        setupWebView()
        //setupProgressLabel()
    }
    
//    func setupProgressLabel() {
//        progressLabel.font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightLight)
//        progressLabel.numberOfLines = 1
//        progressLabel.adjustsFontSizeToFitWidth = true
//        progressLabel.minimumScaleFactor = 0.5
//        progressLabel.textColor = UIColor.white
//        progressLabel.text = "0%"
//        progressLabel.translatesAutoresizingMaskIntoConstraints = false
//        progressLabel.tag = 101
//    }
    
    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: view.frame, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil, let url = navigationAction.request.url {
            if url.description.lowercased().range(of: "http://") != nil ||
                url.description.lowercased().range(of: "https://") != nil ||
                url.description.lowercased().range(of: "mailto:") != nil {
                UIApplication.shared.openURL(url)
            }
        }
        return nil
    }
    
    func setupWebView() {
        let url = URL(string: urlString)
        let req = URLRequest(url: url!)
        webView.layer.backgroundColor = grayMainBg.cgColor
        webView.isOpaque = false
        webView.load(req)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            //let progress = String(Int(webView.estimatedProgress) * 100)
            //progressLabel.text = progress + "%"
            showActivityView(option: true)
            if webView.estimatedProgress == 1.0 {
                showActivityView(option: false)
            }
        }
    }
    
    var allowsMagnification: Bool {
        get {
            return false
        }
    }
    
    func showActivityView(option: Bool) {
        if option {
            if view.viewWithTag(100) == nil {
                let hudView = ActivityView()
                hudView.frame = CGRect(x: webView.bounds.width / 2 - 100 / 2, y: webView.bounds.height / 2 - 100 / 2, width: 100, height: 100)
                hudView.tag = 100
                webView.addSubview(hudView)
                
                //progressLabel.text = "0%"
                //hudView.addSubview(progressLabel)
                
                //let progressCenterX = progressLabel.centerXAnchor.constraint(equalTo: hudView.centerXAnchor)
                //let progressCenterY = progressLabel.topAnchor.constraint(equalTo: hudView.topAnchor, constant: 15)
                //NSLayoutConstraint.activate([progressCenterX, progressCenterY])
            }
        } else {
            if let hudView = view.viewWithTag(100) {
                hudView.removeFromSuperview()
            }
            //if let progressLabel = view.viewWithTag(101) {
            //    progressLabel.removeFromSuperview()
            //}
        }
    }
    
    //MARK: WKNavigationDelegate Methods
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        let url = URL(string: urlString)
        let req = URLRequest(url: url!)
        let alert = UIAlertController(title: "Network Error", message: "There was a problem with the network. " + error.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: "Retry", style: .default, handler: {[unowned self] action in
            self.webView.load(req)})
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {[unowned self] action in
            self.closeAboutViewController(self)})
        alert.addAction(action)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func closeAboutViewController(_ sender: Any) {
        if let navController = navigationController {
            navController.dismiss(animated: true, completion: nil);
        }
    }
    
    deinit {
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
    }

}

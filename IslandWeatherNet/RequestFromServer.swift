//
//  RequestFromServer.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-08-28.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import Foundation
import RealmSwift
//import ReachabilitySwift

protocol RequestFromServerDelegate: class {
    func requestDidReceiveParsedData(_ requestType: RequestDataType, data: NSObject)
    func requestDidFailWithError(_ requestType: RequestDataType, stationTag: String?, error: NSError)
    
}

protocol RequestFromServerParserDelegate {
    func requestDidReceiveRequestData(_ requestType: RequestDataType, data: Data)
}


enum RequestDataType {
    case allStations
    case allObservations
    case observation
}

class RequestFromServer: NSObject, XmlParserDelegate {
    
    var url: URL?
    let requestType: RequestDataType
    let stationTag: String?
    var dataTask: URLSessionDataTask?
    weak var delegate: RequestFromServerDelegate?
    //var network = Reachability()!
    
    required init(requestType: RequestDataType, stationTag: String?) {
        self.requestType = requestType
        self.stationTag = stationTag
        super.init()
        self.url = self.buildUrlFromRequestType(requestType, stationTag: stationTag)
    }
    
    func buildUrlFromRequestType(_ requestType: RequestDataType, stationTag: String?) -> URL {
        let url = "https://viweather.flirky.com/api"
        switch requestType {
            case .observation:
                return URL(string: url + "/station/" + stationTag!)!
            case .allStations:
                return URL(string: url + "/stations.xml")!
            case .allObservations:
                return URL(string: url + "/allcurrent.xml")!
        }
    }
    
    func request() {
        let session = URLSession.shared
        dataTask = session.dataTask(with: url!, completionHandler: { data, response, error in
            if let error = error {
                self.delegate?.requestDidFailWithError(self.requestType, stationTag: self.stationTag, error: error as NSError)
            } else if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                if let data = data {
                    let parser = XmlParser()
                    parser.delegate = self
                    parser.parse(self.requestType, data: data)
                }
            } else if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
                let httpError = NSError(domain: "HTTP", code: httpResponse.statusCode, userInfo: nil)
                self.delegate?.requestDidFailWithError(self.requestType, stationTag: self.stationTag, error: httpError)
            }
        })
        dataTask?.resume()
    }
    
    //MARK: XmlParserDelegate methods.
    
    func xmlParserDidParseData(_ requestType: RequestDataType, data: NSObject) {
        delegate?.requestDidReceiveParsedData(requestType, data: data)
    }
    
    func xmlParserDidFailWithError(_ requestType: RequestDataType, error: NSError) {
        delegate?.requestDidFailWithError(requestType, stationTag: stationTag, error: error)
    }
    
    
}

//
//  temperatureView.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-10-10.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit

//@IBDesignable
class TemperatureView: UIView {
    
    let gray = UIColor.gray.cgColor
    let darkGray = buttonBg.cgColor
    
    let bgThermometer = CAShapeLayer()
    let bgCircle = CAShapeLayer()
    let fgCircle = CAShapeLayer()
    let fgThermometer = CAShapeLayer()
    let thermometerFill = CAShapeLayer()
    let mainMaskLayer = CAShapeLayer()
    let marker10mmLayer = CAShapeLayer()
    
    var bgThermometerWidth: CGFloat = 20 //300
    var bgThermometerHeight: CGFloat = 150
    
    let deviceScaleFactor: CGFloat
    
    var fgThermometerWidth: CGFloat = 14 // 300
    var fgThermometerHeight: CGFloat = 150 // 15
    var bgCircleSize: CGFloat = 40
    var fgCircleSize: CGFloat = 34
    var strokeWidth: CGFloat = 6
    
    var fillPosition: CGPoint = CGPoint(x: 0, y: 0)
    var updateFillPosition = true
    
    var fillValue: CGFloat = 0.0 {
        didSet {
            fillValue *= deviceScaleFactor
            animate()
        }
    }
    
    override public var intrinsicContentSize: CGSize {
        get {
            return CGSize(width: bgCircleSize + strokeWidth, height: bgThermometerHeight + 10)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        if UIScreen.main.bounds.size.height == 480 {
            deviceScaleFactor = 0.60
        } else if UIScreen.main.bounds.size.height == 568 {
            deviceScaleFactor = 0.9
        } else {
            deviceScaleFactor = 1
        }
        bgThermometerHeight *= deviceScaleFactor
        bgThermometerWidth *= deviceScaleFactor
        fgThermometerWidth *= deviceScaleFactor
        fgThermometerHeight *= deviceScaleFactor
        bgCircleSize *= deviceScaleFactor
        fgCircleSize *= deviceScaleFactor
        strokeWidth *= deviceScaleFactor
        super.init(frame: CGRect(x: 0, y: 0, width: bgCircleSize + strokeWidth, height: bgThermometerHeight)) //bgThermometerWidth
    }
    
    func setup() {
        
        let centerX = self.bounds.width / 2
        let centerY = self.bounds.height / 2
        
        //Main mask - large rounded rect to cover all layers.
        mainMaskLayer.fillColor = UIColor.brown.cgColor
        let mainMaskWidth =  bgCircleSize + strokeWidth
        let mainMaskHeight = bgThermometerHeight + strokeWidth - 1
        mainMaskLayer.lineWidth = 0
        let mainMaskPath = UIBezierPath(roundedRect: CGRect(x: centerX - bgCircleSize / 2 - strokeWidth / 2, y: centerY - bgThermometerHeight / 2 - strokeWidth / 2, width: mainMaskWidth, height: mainMaskHeight), cornerRadius: 16) //mainMaskHeight / 2
        mainMaskLayer.path = mainMaskPath.cgPath
        //layer.addSublayer(mainMaskLayer)
        layer.mask = mainMaskLayer
        
        // Dark grey thermometer background
        fgThermometer.fillColor = darkGray
        let fgtPath = UIBezierPath(roundedRect: CGRect(x: centerX - fgThermometerWidth / 2, y: centerY - fgThermometerHeight / 2, width: fgThermometerWidth, height: fgThermometerHeight), cornerRadius: 10)
        fgThermometer.path = fgtPath.cgPath
        layer.addSublayer(fgThermometer)
        
        // Large circle
        bgCircle.strokeColor = gray
        bgCircle.fillColor = darkGray
        bgCircle.lineWidth = strokeWidth
        let bgcPath = UIBezierPath(ovalIn: CGRect(x: centerX - bgCircleSize / 2, y: centerY + bgThermometerHeight / 2 - bgCircleSize, width: bgCircleSize, height: bgCircleSize))
        bgCircle.path = bgcPath.cgPath
        layer.addSublayer(bgCircle)
        
        // Thermometer Fill
        thermometerFill.fillColor = orange
        let tfPath = UIBezierPath(roundedRect: CGRect(x: centerX - fgThermometerWidth / 2, y: centerY + fgThermometerHeight / 2 - bgCircleSize - strokeWidth / 2, width: fgThermometerWidth, height: fgThermometerHeight), cornerRadius: 0)
        thermometerFill.path = tfPath.cgPath
        layer.addSublayer(thermometerFill)
        if updateFillPosition {
            fillPosition = thermometerFill.position
            updateFillPosition = false
        }
        
        // Thermometer outline
        bgThermometer.fillColor = UIColor.clear.cgColor
        bgThermometer.strokeColor = gray
        bgThermometer.lineWidth = strokeWidth
        let bgtPath = UIBezierPath(roundedRect: CGRect(x: centerX - bgThermometerWidth / 2, y: centerY - bgThermometerHeight / 2, width: bgThermometerWidth, height: bgThermometerHeight), cornerRadius: 10)
        bgThermometer.path = bgtPath.cgPath
        layer.addSublayer(bgThermometer)
        
        // Small Circle
        fgCircle.fillColor = orange
        let fgcPath = UIBezierPath(ovalIn: CGRect(x: centerX - fgCircleSize / 2, y: centerY + bgThermometerHeight / 2 - strokeWidth / 2 - fgCircleSize, width: fgCircleSize, height: fgCircleSize))
        fgCircle.path = fgcPath.cgPath
        layer.addSublayer(fgCircle)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    func animate() {
        
        let positionTo = fillPosition.y - fillValue
        let moveBar = CABasicAnimation(keyPath: "position.y")
        moveBar.fromValue = fillPosition.y
        moveBar.toValue = positionTo
        moveBar.duration = 1.0
        
        thermometerFill.add(moveBar, forKey: nil)
        CATransaction.begin()
        //CATransaction.setDisableActions(true)
        thermometerFill.position.y = positionTo
        CATransaction.commit()
    }

}

//
//  Settings.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-07-21.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import Foundation
import RealmSwift

class Settings: Object {
    
    dynamic var useNearestStation = true
    dynamic var station = "UVic Science Building"
    dynamic var stationTag = "UVicSci"
    dynamic var latitude = 48.462299
    dynamic var longitude = -123.643005
    
}

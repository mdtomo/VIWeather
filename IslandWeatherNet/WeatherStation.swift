//
//  WeatherStation.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-08-03.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import Foundation
import RealmSwift

class WeatherStation: Object {
    
    dynamic var name = ""
    dynamic var id = 0 // NSUID().UUIDString for a unique id.
    dynamic var latitude = 0.0
    dynamic var longitude = 0.0
    dynamic var elevation = 0
    dynamic var tag = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    override class func indexedProperties() -> [String] {
        return ["name"]
    }
    
}
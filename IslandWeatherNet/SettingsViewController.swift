//
//  SettingsViewController.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-07-21.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit
import RealmSwift

class SettingsViewController: UITableViewController { 
    
    var settings: Results<Settings>!

    @IBOutlet weak var useNearestStationSwitch: UISwitch!
    @IBOutlet weak var chosenStation: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = grayMainBg
        tableView.separatorColor = grayTableSeparator
        tableView.indicatorStyle = .white
        tableView.tableFooterView = UIView()
        loadSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        useNearestStationSwitch.isOn = settings[0].useNearestStation
        chosenStation.text = settings[0].station
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    func loadSettings() {
        do {
            let realm = try Realm()
            settings = realm.objects(Settings.self)
        } catch {
            print("Something went wrong!" + "\(error)")
        }
    }
    
    @IBAction func useNearestStation(_ sender: UISwitch) {
        do {
            let realm = try Realm()
            try realm.write {
                settings[0].useNearestStation = useNearestStationSwitch.isOn
            }
        } catch {
            print("Error in saving switch state." + "\(error)")
        }
    }
    
    // MARK: - TableView
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier! == "PickStation" {
            let psvc = segue.destination as! PickStationViewController
            psvc.title = "Find Station"
        } else if segue.identifier! == "Feedback" {
            let navController = segue.destination as! CustomNavigationController
            let aboutVc = navController.viewControllers[0] as! AboutViewController
            aboutVc.title = "Feedback"
            aboutVc.urlString = "https://viweather.flirky.com/feedback"
        }
    }

}

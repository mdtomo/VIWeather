//
//  ScaleView.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-09-26.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit

//@IBDesignable
class ScaleView: UIView {
    
    override public var intrinsicContentSize: CGSize {
        get {
            return CGSize(width: circleSize + circleWidth, height: circleSize + circleWidth)
        }
    }
    
    let label = UILabel()
    var labelValue = ""
    
    @IBInspectable var range: CGFloat = 100 //10
    var curValue: CGFloat = 0 {
        didSet {
            animate()
        }
    }
    
    let bgLayer = CAShapeLayer()
    @IBInspectable var bgColor: UIColor = UIColor.gray {
        didSet {
            configure()
        }
    }
    let fgLayer = CAShapeLayer()
    @IBInspectable var fgColor: UIColor = UIColor.black {
        didSet {
            configure()
        }
    }
 
    @IBInspectable var circleWidth: CGFloat = 10.0 {
        didSet {
            configure()
        }
    }
    
    @IBInspectable var circleSize: CGFloat = 100.0 { // 160
        didSet {
            configure()
        }
    }
    
    let deviceScaleFactor: CGFloat
    var fontSize: CGFloat = 18
    
    required init?(coder aDecoder: NSCoder) {
        if UIScreen.main.bounds.size.height == 480 {
            deviceScaleFactor = 0.6
            fontSize = 10
            //circleWidth = 1 Set to 6.0 by interface builder
        } else if UIScreen.main.bounds.size.height == 568 {
            deviceScaleFactor = 0.9
            fontSize = 16
        } else {
            deviceScaleFactor = 1
        }
        circleSize *= deviceScaleFactor
        super.init(frame: CGRect(x: 0, y: 0, width: circleSize + circleWidth, height: circleSize + circleWidth))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        configure()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
        configure()
    }
 
    func setup() {
        
        // Setup background layer
        bgLayer.lineWidth = circleWidth
        bgLayer.fillColor = buttonBg.cgColor
        bgLayer.strokeEnd = 1
        layer.addSublayer(bgLayer)
        fgLayer.lineWidth = circleWidth
        fgLayer.fillColor = nil
        fgLayer.strokeEnd = 0
        layer.addSublayer(fgLayer)
        self.clipsToBounds = true
    }
    
    func configure() {
        bgLayer.strokeColor = bgColor.cgColor
        fgLayer.strokeColor = fgColor.cgColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupShapeLayer(shapeLayer: bgLayer)
        setupShapeLayer(shapeLayer: fgLayer)
        setupLabel()
        
    }
    
    private func setupShapeLayer(shapeLayer:CAShapeLayer) {
        shapeLayer.frame = self.bounds
        let startAngle = DegreesToRadians(value: 90.0) // 135.0
        let endAngle = DegreesToRadians(value: 450.0) // 45.0
        let center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
        let radius = circleSize / 2
        let path = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        shapeLayer.path = path.cgPath
        
    }
    
    private func setupLabel() {
        // Setup label
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.font = UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightThin)
        label.textColor = UIColor.white
        label.text = labelValue
        label.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(label)
        
        let labelCenterX = label.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        let labelCenterY = label.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        NSLayoutConstraint.activate([labelCenterX, labelCenterY])
    }
    
    private func animate() {
        label.text = labelValue
        let fromValue: CGFloat = 0
        let toValue = curValue / range
        let percentChange = abs(fromValue - toValue)
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.duration = CFTimeInterval(percentChange * 1.5)
        fgLayer.removeAnimation(forKey: "stroke")
        fgLayer.add(animation, forKey: "stroke")
        
        CATransaction.begin()
        fgLayer.strokeEnd = toValue
        CATransaction.commit()
    }

}

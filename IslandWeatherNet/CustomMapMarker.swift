//
//  CustomMapMarker.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-09-17.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import Foundation

@objc class CustomMapMarker: NSObject {
    
    static func createMarker(_ temperature: String, humidity: String, isCluster: Bool) -> UIImage {
        var hasNoData = false
        
        if temperature == "--°" && humidity == "--%" {
            hasNoData = true
        }
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 32, height: 32), false, 0)
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()
        
        //// Color Declarations
        //let darkGreen = UIColor(red: 0.128, green: 0.315, blue: 0.165, alpha: 1.000)
        //let lightGreen = UIColor(red: 0.470, green: 0.575, blue: 0.233, alpha: 1.000)
        
        //let darkBlue = UIColor(red: 0.270, green: 0.333, blue: 0.498, alpha: 1.000)
        //let lightBlue = UIColor(red: 0.310, green: 0.612, blue: 0.773, alpha: 1.000)
        
        let darkGrey = UIColor(red: 0.342, green: 0.336, blue: 0.336, alpha: 1.000)
        let lightGrey = UIColor(red: 0.610, green: 0.610, blue: 0.610, alpha: 1.000)
        
        //let darkOrange = UIColor(red: 0.784, green: 0.376, blue: 0.000, alpha: 1.000)
        //let lightOrange = UIColor(red: 1.000, green: 0.675, blue: 0.373, alpha: 1.000)
        
        //let darkRed = UIColor(red: 1.000, green: 0.039, blue: 0.039, alpha: 1.000)
        //let lightRed = UIColor(red: 1.000, green: 0.373, blue: 0.373, alpha: 1.000)
        
        let strokeColor = UIColor(red: 0.190, green: 0.190, blue: 0.190, alpha: 1.000)
        
        //// Gradient Declarations
        //let greenGradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [darkGreen.cgColor, lightGreen.cgColor] as CFArray, locations: [0, 1])!
        let greyGradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [lightGrey.cgColor, darkGrey.cgColor] as CFArray, locations: [0, 1])!
        //let blueGradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [darkBlue.cgColor, lightBlue.cgColor] as CFArray, locations: [0, 1])!
        //let orangeGradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [darkOrange.cgColor, lightOrange.cgColor] as CFArray, locations: [0, 1])!
        //let redGradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [darkRed.cgColor, lightRed.cgColor] as CFArray, locations: [0, 1])!
        let tealGradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [teal.cgColor, darkTeal.cgColor] as CFArray, locations: [0, 1])!
        
        
        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 27.52, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 27.6, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 27.68, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 27.75, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 27.82, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 27.89, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 27.96, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 28.03, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 28.09, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 28.15, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 28.21, y: 1.51))
        bezierPath.addLine(to: CGPoint(x: 28.27, y: 1.51))
        bezierPath.addLine(to: CGPoint(x: 28.32, y: 1.51))
        bezierPath.addLine(to: CGPoint(x: 28.38, y: 1.51))
        bezierPath.addLine(to: CGPoint(x: 28.43, y: 1.51))
        bezierPath.addLine(to: CGPoint(x: 28.48, y: 1.52))
        bezierPath.addCurve(to: CGPoint(x: 30.35, y: 2.78), controlPoint1: CGPoint(x: 29.75, y: 1.84), controlPoint2: CGPoint(x: 30.16, y: 2.26))
        bezierPath.addCurve(to: CGPoint(x: 30.39, y: 2.92), controlPoint1: CGPoint(x: 30.36, y: 2.83), controlPoint2: CGPoint(x: 30.38, y: 2.87))
        bezierPath.addLine(to: CGPoint(x: 30.48, y: 3.49))
        bezierPath.addLine(to: CGPoint(x: 30.48, y: 3.54))
        bezierPath.addLine(to: CGPoint(x: 30.48, y: 3.6))
        bezierPath.addLine(to: CGPoint(x: 30.49, y: 3.65))
        bezierPath.addLine(to: CGPoint(x: 30.49, y: 3.71))
        bezierPath.addLine(to: CGPoint(x: 30.49, y: 3.77))
        bezierPath.addLine(to: CGPoint(x: 30.49, y: 3.83))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 3.89))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 3.95))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 4.01))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 4.08))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 4.15))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 4.22))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 4.29))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 4.37))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 4.45))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 4.53))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 4.61))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 23.81))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 23.9))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 23.98))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 24.05))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 24.13))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 24.2))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 24.27))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 24.34))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 24.41))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 24.47))
        bezierPath.addLine(to: CGPoint(x: 30.5, y: 24.54))
        bezierPath.addLine(to: CGPoint(x: 30.49, y: 24.6))
        bezierPath.addLine(to: CGPoint(x: 30.49, y: 24.65))
        bezierPath.addLine(to: CGPoint(x: 30.49, y: 24.71))
        bezierPath.addLine(to: CGPoint(x: 30.49, y: 24.77))
        bezierPath.addLine(to: CGPoint(x: 30.49, y: 24.82))
        bezierPath.addLine(to: CGPoint(x: 30.48, y: 24.87))
        bezierPath.addCurve(to: CGPoint(x: 29.24, y: 26.77), controlPoint1: CGPoint(x: 30.16, y: 26.17), controlPoint2: CGPoint(x: 29.75, y: 26.58))
        bezierPath.addLine(to: CGPoint(x: 28.54, y: 26.9))
        bezierPath.addLine(to: CGPoint(x: 28.49, y: 26.9))
        bezierPath.addLine(to: CGPoint(x: 28.44, y: 26.91))
        bezierPath.addLine(to: CGPoint(x: 28.39, y: 26.91))
        bezierPath.addLine(to: CGPoint(x: 28.33, y: 26.91))
        bezierPath.addLine(to: CGPoint(x: 28.27, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 28.21, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 28.15, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 28.09, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 28.03, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 27.96, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 27.89, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 27.82, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 27.75, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 27.68, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 27.6, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 27.52, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 27.44, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 20.45, y: 26.92))
        bezierPath.addCurve(to: CGPoint(x: 15.95, y: 31.5), controlPoint1: CGPoint(x: 19.23, y: 28.16), controlPoint2: CGPoint(x: 15.95, y: 31.5))
        bezierPath.addCurve(to: CGPoint(x: 11.45, y: 26.92), controlPoint1: CGPoint(x: 15.95, y: 31.5), controlPoint2: CGPoint(x: 12.67, y: 28.16))
        bezierPath.addLine(to: CGPoint(x: 3.56, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 3.48, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 3.4, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 3.32, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 3.25, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 3.18, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 3.11, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 3.04, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 2.97, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 2.91, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 2.85, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 2.79, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 2.73, y: 26.92))
        bezierPath.addLine(to: CGPoint(x: 2.68, y: 26.91))
        bezierPath.addLine(to: CGPoint(x: 2.62, y: 26.91))
        bezierPath.addLine(to: CGPoint(x: 2.57, y: 26.91))
        bezierPath.addLine(to: CGPoint(x: 2.52, y: 26.91))
        bezierPath.addCurve(to: CGPoint(x: 0.65, y: 25.64), controlPoint1: CGPoint(x: 1.25, y: 26.58), controlPoint2: CGPoint(x: 0.84, y: 26.17))
        bezierPath.addCurve(to: CGPoint(x: 0.5, y: 23.9), controlPoint1: CGPoint(x: 0.5, y: 25.17), controlPoint2: CGPoint(x: 0.5, y: 24.74))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 23.81))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 4.61))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 4.53))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 4.45))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 4.37))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 4.29))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 4.22))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 4.15))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 4.08))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 4.02))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 3.95))
        bezierPath.addLine(to: CGPoint(x: 0.5, y: 3.89))
        bezierPath.addLine(to: CGPoint(x: 0.51, y: 3.83))
        bezierPath.addLine(to: CGPoint(x: 0.51, y: 3.77))
        bezierPath.addLine(to: CGPoint(x: 0.51, y: 3.71))
        bezierPath.addLine(to: CGPoint(x: 0.51, y: 3.66))
        bezierPath.addLine(to: CGPoint(x: 0.51, y: 3.6))
        bezierPath.addLine(to: CGPoint(x: 0.52, y: 3.55))
        bezierPath.addCurve(to: CGPoint(x: 1.76, y: 1.65), controlPoint1: CGPoint(x: 0.84, y: 2.26), controlPoint2: CGPoint(x: 1.25, y: 1.84))
        bezierPath.addLine(to: CGPoint(x: 2.46, y: 1.52))
        bezierPath.addLine(to: CGPoint(x: 2.51, y: 1.52))
        bezierPath.addLine(to: CGPoint(x: 2.56, y: 1.52))
        bezierPath.addLine(to: CGPoint(x: 2.61, y: 1.51))
        bezierPath.addLine(to: CGPoint(x: 2.67, y: 1.51))
        bezierPath.addLine(to: CGPoint(x: 2.73, y: 1.51))
        bezierPath.addLine(to: CGPoint(x: 2.79, y: 1.51))
        bezierPath.addLine(to: CGPoint(x: 2.85, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 2.91, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 2.97, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 3.04, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 3.11, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 3.18, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 3.25, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 3.32, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 3.4, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 3.48, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 3.56, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 27.44, y: 1.5))
        bezierPath.addLine(to: CGPoint(x: 27.52, y: 1.5))
        bezierPath.close()
        bezierPath.lineCapStyle = .round;
        
        bezierPath.lineJoinStyle = .round;
        
        context?.saveGState()
        bezierPath.addClip()
        if hasNoData {
            context?.drawLinearGradient(greyGradient, start: CGPoint(x: 15.5, y: 1.5), end: CGPoint(x: 15.5, y: 31.5), options: CGGradientDrawingOptions())
        } else if isCluster {
            context?.drawLinearGradient(tealGradient, start: CGPoint(x: 15.5, y: 1.5), end: CGPoint(x: 15.5, y: 31.5), options: CGGradientDrawingOptions())
        } else {
            context?.drawLinearGradient(tealGradient, start: CGPoint(x: 15.5, y: 1.5), end: CGPoint(x: 15.5, y: 31.5), options: CGGradientDrawingOptions())
        }
        context?.restoreGState()
        strokeColor.setStroke()
        bezierPath.lineWidth = 1
        bezierPath.stroke()
        
        //Create No Data text fields instead of data.
        if hasNoData {
            //No field
            let noRect = CGRect(x: 1, y: 4, width: 28, height: 10)
            let noTextContent = NSString(string: "No")
            let noStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            noStyle.alignment = .center
            
            let noFontAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 8), NSForegroundColorAttributeName: markerTxt, NSParagraphStyleAttributeName: noStyle]
            
            let noTextHeight: CGFloat = noTextContent.boundingRect(with: CGSize(width: noRect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: noFontAttributes, context: nil).size.height
            context?.saveGState()
            context?.clip(to: noRect);
            noTextContent.draw(in: CGRect(x: noRect.minX, y: noRect.minY + (noRect.height - noTextHeight) / 2, width: noRect.width, height: noTextHeight), withAttributes: noFontAttributes)
            context?.restoreGState()
            
            //Data field
            let dataRect = CGRect(x: 1, y: 14, width: 28, height: 10)
            let dataTextContent = NSString(string: "Data")
            let dataStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            dataStyle.alignment = .center
            
            let dataFontAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 8), NSForegroundColorAttributeName: markerTxt, NSParagraphStyleAttributeName: dataStyle]
            
            let dataTextHeight: CGFloat = dataTextContent.boundingRect(with: CGSize(width: dataRect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: dataFontAttributes, context: nil).size.height
            context?.saveGState()
            context?.clip(to: dataRect);
            dataTextContent.draw(in: CGRect(x: dataRect.minX, y: dataRect.minY + (dataRect.height - dataTextHeight) / 2, width: dataRect.width, height: dataTextHeight), withAttributes: dataFontAttributes)
            context?.restoreGState()
        } else if isCluster {
            // Number of stations in cluster field
            let noRect = CGRect(x: 1, y: 4, width: 28, height: 11)
            let noTextContent = NSString(string: temperature)
            let noStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            noStyle.alignment = .center
            
            let noFontAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 8), NSForegroundColorAttributeName: markerTxt, NSParagraphStyleAttributeName: noStyle]
            
            let noTextHeight: CGFloat = noTextContent.boundingRect(with: CGSize(width: noRect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: noFontAttributes, context: nil).size.height
            context?.saveGState()
            context?.clip(to: noRect);
            noTextContent.draw(in: CGRect(x: noRect.minX, y: noRect.minY + (noRect.height - noTextHeight) / 2, width: noRect.width, height: noTextHeight), withAttributes: noFontAttributes)
            context?.restoreGState()
            
            //Stations field
            let dataRect = CGRect(x: 1, y: 14, width: 28, height: 10)
            let dataTextContent = NSString(string: "Stations")
            let dataStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            dataStyle.alignment = .center
            
            let dataFontAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 6), NSForegroundColorAttributeName: markerTxt, NSParagraphStyleAttributeName: dataStyle]
            
            let dataTextHeight: CGFloat = dataTextContent.boundingRect(with: CGSize(width: dataRect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: dataFontAttributes, context: nil).size.height
            context?.saveGState()
            context?.clip(to: dataRect);
            dataTextContent.draw(in: CGRect(x: dataRect.minX, y: dataRect.minY + (dataRect.height - dataTextHeight) / 2, width: dataRect.width, height: dataTextHeight), withAttributes: dataFontAttributes)
            context?.restoreGState()
        } else {
            //// temperaturePlace Drawing
            let temperaturePlaceRect = CGRect(x: 0, y: 4, width: 31, height: 10)
            let temperaturePlaceStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            temperaturePlaceStyle.alignment = .center
            
            let temperaturePlaceFontAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 8), NSForegroundColorAttributeName: UIColor.white, NSParagraphStyleAttributeName: temperaturePlaceStyle]
            
            let temperaturePlaceTextHeight: CGFloat = NSString(string: temperature).boundingRect(with: CGSize(width: temperaturePlaceRect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: temperaturePlaceFontAttributes, context: nil).size.height
            context!.saveGState()
            context!.clip(to: temperaturePlaceRect);
            NSString(string: temperature).draw(in: CGRect(x: temperaturePlaceRect.minX, y: temperaturePlaceRect.minY + (temperaturePlaceRect.height - temperaturePlaceTextHeight) / 2, width: temperaturePlaceRect.width, height: temperaturePlaceTextHeight), withAttributes: temperaturePlaceFontAttributes)
            context!.restoreGState()
            
            
            //// humidityPlace Drawing
            let humidityPlaceRect = CGRect(x: 0, y: 14, width: 31, height: 10)
            let humidityPlaceStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            humidityPlaceStyle.alignment = .center
            
            let humidityPlaceFontAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 8), NSForegroundColorAttributeName: UIColor.white, NSParagraphStyleAttributeName: humidityPlaceStyle]
            
            let humidityPlaceTextHeight: CGFloat = NSString(string: humidity).boundingRect(with: CGSize(width: humidityPlaceRect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: humidityPlaceFontAttributes, context: nil).size.height
            context!.saveGState()
            context!.clip(to: humidityPlaceRect);
            NSString(string: humidity).draw(in: CGRect(x: humidityPlaceRect.minX, y: humidityPlaceRect.minY + (humidityPlaceRect.height - humidityPlaceTextHeight) / 2, width: humidityPlaceRect.width, height: humidityPlaceTextHeight), withAttributes: humidityPlaceFontAttributes)
            context!.restoreGState()
        }
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    static func createArrow(isUp: Bool, color1: CGColor, color2: CGColor) -> UIImage {
    
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 27, height: 27), false, 0)
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()
        //// Gradient Declarations
        let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [color1, color2] as CFArray, locations: [0, 1])!
        
        if isUp { //// Arrow Up
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 13.66, y: 3.5))
            bezierPath.addCurve(to: CGPoint(x: 21.5, y: 13.5), controlPoint1: CGPoint(x: 13.66, y: 3.5), controlPoint2: CGPoint(x: 21.5, y: 13.5))
            bezierPath.addLine(to: CGPoint(x: 17.44, y: 13.5))
            bezierPath.addCurve(to: CGPoint(x: 17.44, y: 23.5), controlPoint1: CGPoint(x: 17.44, y: 14.91), controlPoint2: CGPoint(x: 17.44, y: 23.5))
            bezierPath.addLine(to: CGPoint(x: 10.11, y: 23.5))
            bezierPath.addCurve(to: CGPoint(x: 10.11, y: 13.5), controlPoint1: CGPoint(x: 10.11, y: 23.5), controlPoint2: CGPoint(x: 10.11, y: 14.91))
            bezierPath.addLine(to: CGPoint(x: 5.82, y: 13.5))
            bezierPath.addCurve(to: CGPoint(x: 13.66, y: 3.5), controlPoint1: CGPoint(x: 5.82, y: 13.5), controlPoint2: CGPoint(x: 13.66, y: 3.5))
            bezierPath.addLine(to: CGPoint(x: 13.66, y: 3.5))
            bezierPath.close()
            context!.saveGState()
            bezierPath.addClip()
            context!.drawLinearGradient(gradient, start: CGPoint(x: 13.66, y: 3.5), end: CGPoint(x: 13.66, y: 23.5), options: CGGradientDrawingOptions())
            context!.restoreGState()
            UIColor.black.setStroke()
            bezierPath.lineWidth = 1
            bezierPath.stroke()
        } else { //// Arrow Down
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 13.66, y: 23.5))
            bezierPath.addCurve(to: CGPoint(x: 21.5, y: 13.5), controlPoint1: CGPoint(x: 13.66, y: 23.5), controlPoint2: CGPoint(x: 21.5, y: 13.5))
            bezierPath.addLine(to: CGPoint(x: 17.44, y: 13.5))
            bezierPath.addCurve(to: CGPoint(x: 17.44, y: 3.5), controlPoint1: CGPoint(x: 17.44, y: 12.09), controlPoint2: CGPoint(x: 17.44, y: 3.5))
            bezierPath.addLine(to: CGPoint(x: 10.11, y: 3.5))
            bezierPath.addCurve(to: CGPoint(x: 10.11, y: 13.5), controlPoint1: CGPoint(x: 10.11, y: 3.5), controlPoint2: CGPoint(x: 10.11, y: 12.09))
            bezierPath.addLine(to: CGPoint(x: 5.82, y: 13.5))
            bezierPath.addCurve(to: CGPoint(x: 13.66, y: 23.5), controlPoint1: CGPoint(x: 5.82, y: 13.5), controlPoint2: CGPoint(x: 13.66, y: 23.5))
            bezierPath.addLine(to: CGPoint(x: 13.66, y: 23.5))
            bezierPath.close()
            context!.saveGState()
            bezierPath.addClip()
            context!.drawLinearGradient(gradient, start: CGPoint(x: 13.66, y: 23.5), end: CGPoint(x: 13.66, y: 3.5), options: CGGradientDrawingOptions())
            context!.restoreGState()
            UIColor.black.setStroke()
            bezierPath.lineWidth = 1
            bezierPath.stroke()
        }
    
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    
    }
}

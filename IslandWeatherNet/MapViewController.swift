//
//  MapViewController.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-07-21.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit
import RealmSwift
import GoogleMaps
import Crashlytics

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class MapViewController: UIViewController, RequestFromServerDelegate {

    var settings: Results<Settings>!
    var allStations: Results<WeatherStation>!
    var allObservations = [WeatherObservation]()
    lazy var selectedObservation = WeatherObservation()
    
    var initialLocation: CLLocation?
    var currentLocation: CLLocation?
    var locationManager = CLLocationManager()
    
    fileprivate var clusterManager: GMUClusterManager!

    var isBarOpen = false
    var isSummaryOpen = false
    var moreStationInfo = false
    var temperatureList: [WeatherObservation]?
    var humidityList: [WeatherObservation]?
    var pressureList: [WeatherObservation]?
    var windList: [WeatherObservation]?
    var rainList: [WeatherObservation]?
    var summaryModeHighest = true
    var arrowMarkers = [GMSMarker]()
    var clearButtonEnabled = false
    var locationButtonEnabled = false
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var stationView: UIView!
    @IBOutlet weak var stationTitleBar: UIView!
    @IBOutlet weak var stationContentsView: UIView!
    @IBOutlet weak var stationStackView: UIStackView!
    
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var stationName: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var tempHigh: UILabel!
    @IBOutlet weak var tempLow: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var pressureTrend: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var windMax: UILabel!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var rain: UILabel!
    @IBOutlet weak var rainRate: UILabel!
    @IBOutlet weak var evapotranspiration: UILabel!
    @IBOutlet weak var dewPoint: UILabel!
    @IBOutlet weak var uvIndex: UILabel!
    @IBOutlet weak var insolation: UILabel!
    @IBOutlet weak var insolationPredicted: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var elevation: UILabel!
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var tempHiLabel: UILabel!
    @IBOutlet weak var tempLowLabel: UILabel!
    @IBOutlet weak var dewpointLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var windMaxLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var pressureTrendLabel: UILabel!
    @IBOutlet weak var uvindexLabel: UILabel!
    @IBOutlet weak var insolationLabel: UILabel!
    @IBOutlet weak var insolationPredictedLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var elevationLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var rainRateLabel: UILabel!
    @IBOutlet weak var evapotranspirationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var temperatureHeight: NSLayoutConstraint! //16
    @IBOutlet weak var humidityHeight: NSLayoutConstraint! //13
    @IBOutlet weak var windHeight: NSLayoutConstraint!
    @IBOutlet weak var windMaxHeight: NSLayoutConstraint!
    @IBOutlet weak var headingHeight: NSLayoutConstraint!
    @IBOutlet weak var insolationHeight: NSLayoutConstraint!
    @IBOutlet weak var insolationPredictedHeight: NSLayoutConstraint!
    @IBOutlet weak var rainHeight: NSLayoutConstraint!
    @IBOutlet weak var rateHeight: NSLayoutConstraint!
    @IBOutlet weak var evapotranspirationHeight: NSLayoutConstraint!
    @IBOutlet weak var timeHeight: NSLayoutConstraint!
    @IBOutlet weak var setStationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var summaryHiLo: UISegmentedControl!
    @IBOutlet weak var summaryTemperature: UILabel!
    @IBOutlet weak var summaryHumidity: UILabel!
    @IBOutlet weak var summaryPressure: UILabel!
    @IBOutlet weak var summaryWind: UILabel!
    @IBOutlet weak var summaryRain: UILabel!
    @IBOutlet weak var summaryTemperatureStation: UIButton!
    @IBOutlet weak var summaryHumidityStation: UIButton!
    @IBOutlet weak var summaryPressureStation: UIButton!
    @IBOutlet weak var summaryWindStation: UIButton!
    @IBOutlet weak var summaryRainStation: UIButton!
    
    @IBOutlet weak var temperatureContainer: UIView!
    @IBOutlet weak var humidityContainer: UIView!
    @IBOutlet weak var pressureContainer: UIView!
    @IBOutlet weak var windContainer: UIView!
    @IBOutlet weak var rainContainer: UIView!
    
    @IBOutlet weak var stationDefaultButton: UIButton!
    @IBOutlet weak var stationMoreLessButton: UIButton!
    @IBOutlet weak var stationCloseButton: UIButton!
    
    @IBOutlet weak var dewPointContainer: UIView!
    @IBOutlet weak var uvContainer: UIView!
    @IBOutlet weak var insolationContainer: UIView!
    @IBOutlet weak var distanceContainer: UIView!
    @IBOutlet weak var timeContainer: UIView!
    @IBOutlet weak var uvIndexInsolationStackView: UIStackView!
    @IBOutlet weak var distanceRainStackView: UIStackView!
    @IBOutlet weak var windPressureStackView: UIStackView!
    
    @IBOutlet weak var summaryViewStackView: UIStackView!
    @IBOutlet weak var summaryView: UIView!
    @IBOutlet weak var summaryTitleBar: UIView!
    
    @IBOutlet weak var toggleSummaryButton: UIButton!
    @IBOutlet weak var summaryListStackView: UIStackView!
    
    @IBOutlet weak var tempArrow: UIImageView!
    @IBOutlet weak var humidityArrow: UIImageView!
    @IBOutlet weak var pressureArrow: UIImageView!
    @IBOutlet weak var windArrow: UIImageView!
    @IBOutlet weak var rainArrow: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSettings()
        mapView.delegate = self
        mapView.mapType = kGMSTypeTerrain
        // Setup map clusters
        let iconGenerator = self//GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView, clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm, renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
        initialLocation = CLLocation(latitude: 49.552579, longitude: -125.685086)// Center Island Roughly 49.552579, -125.685086
        if initialLocation != nil {
            centerMap(initialLocation!, zoomLevel: 6.5)
        }
        addLocationButton(enabled: false)
        NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedToBackground), name: .UIApplicationWillResignActive, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if allObservations.first?.observationTime == nil {
            initLabels()
        }
        setupGradients()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateMap()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func didBecomeActive() {
        updateMap()
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidBecomeActive, object: nil)
    }
    
    func appMovedToBackground() {
        if self.isViewLoaded && self.view.window != nil {
            NotificationCenter.default.addObserver(self, selector: #selector(self.didBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
            //Remove arrow markers
            if arrowMarkers.count > 0 {
                clearButtonTap(sender: UIButton())
            }
            temperatureList?.removeAll()
            humidityList?.removeAll()
            pressureList?.removeAll()
            windList?.removeAll()
            rainList?.removeAll()
            allObservations.removeAll()
        }
    }
    
    func addLocationButton(enabled: Bool) {
        let locationButton = UIButton(type: .custom)
        locationButton.translatesAutoresizingMaskIntoConstraints = false
        locationButton.backgroundColor = buttonBg
        locationButton.setBackgroundImage(CustomMapButton.location(enabled: enabled), for: UIControlState())
        locationButton.layer.cornerRadius = 4.0
        locationButton.layer.shadowRadius = 1.0
        locationButton.layer.shadowOffset = CGSize(width: 1, height: 1)
        locationButton.layer.shadowColor = UIColor.black.cgColor
        locationButton.layer.shadowOpacity = 1.0
        locationButton.tag = 102
        locationButton.addTarget(self, action: #selector(self.locationButtonTap), for: .touchUpInside)
        mapView.addSubview(locationButton)
        
        let margin: CGFloat = 10.0
        let buttonSize: CGFloat = 44.0
        
        let locationButtonWidth = locationButton.widthAnchor.constraint(equalToConstant: buttonSize)
        let locationButtonHeight = locationButton.heightAnchor.constraint(equalToConstant: buttonSize)
        let locationButtonTrailingX = locationButton.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -5)
        let locationButtonBottomY = locationButton.bottomAnchor.constraint(equalTo: mapView.bottomAnchor, constant: -margin)
        
        NSLayoutConstraint.activate([locationButtonWidth, locationButtonHeight, locationButtonTrailingX, locationButtonBottomY])
    }
    
    func addClearButton() {
        let clearButton = UIButton(type: .custom)
        clearButton.translatesAutoresizingMaskIntoConstraints = false
        clearButton.backgroundColor = buttonBg
        clearButton.setBackgroundImage(CustomMapButton.clear(), for: UIControlState())
        clearButton.layer.cornerRadius = 4.0
        clearButton.layer.shadowRadius = 1.0
        clearButton.layer.shadowOffset = CGSize(width: 1, height: 1)
        clearButton.layer.shadowColor = UIColor.black.cgColor
        clearButton.layer.shadowOpacity = 1.0
        clearButton.tag = 103
        clearButton.addTarget(self, action: #selector(self.clearButtonTap), for: .touchUpInside)
        self.mapView.addSubview(clearButton)
        
        let margin: CGFloat = 10.0
        let buttonSize: CGFloat = 44.0
        
        let clearButtonWidth = clearButton.widthAnchor.constraint(equalToConstant: buttonSize)
        let clearButtonHeight = clearButton.heightAnchor.constraint(equalToConstant: buttonSize)
        let clearButtonTrailingX = clearButton.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -margin - buttonSize)
        let clearButtonBottomY = clearButton.bottomAnchor.constraint(equalTo: mapView.bottomAnchor, constant: -margin)
        
        NSLayoutConstraint.activate([clearButtonWidth, clearButtonHeight, clearButtonTrailingX, clearButtonBottomY])
        
        let animateOpacity = CABasicAnimation(keyPath: "opacity")
        animateOpacity.fromValue = 0.0
        animateOpacity.toValue = 1.0
        animateOpacity.duration = 0.5
        
        clearButton.layer.add(animateOpacity, forKey: nil)
    }
    
    @IBAction func locationButtonTap(sender: UIButton) {
        locationButtonEnabled = !locationButtonEnabled
        let locationButton = mapView.viewWithTag(102)
        if locationButtonEnabled {
            checkLocationAuthorizationStatus()
            locationButton?.removeFromSuperview()
            addLocationButton(enabled: true)
        } else {
            mapView.isMyLocationEnabled = false
            locationButton?.removeFromSuperview()
            addLocationButton(enabled: false)
        }
    }
    
    @IBAction func clearButtonTap(sender: UIButton) {
        for arrow in arrowMarkers {
            arrow.map = nil
        }
        arrowMarkers.removeAll()
        if let clearButton = view.viewWithTag(103) {
            clearButton.removeFromSuperview()
        }
        clearButtonEnabled = false
    }
    
    func updateMap() {
        if let time = allObservations.first?.observationTime {
            if dataNeedsUpdating(observationTime: time) {
                clusterManager.clearItems()
                showActivityView(option: true)
                let req = RequestFromServer(requestType: .allObservations, stationTag: nil)
                req.delegate = self
                req.request()
            }
        } else {
            showActivityView(option: true)
            let req = RequestFromServer(requestType: .allObservations, stationTag: nil)
            req.delegate = self
            req.request()
        }
    }
    
    func showActivityView(option: Bool) {
        if option {
            let hudView = ActivityView()
            hudView.frame = CGRect(x: view.bounds.width / 2 - 100 / 2, y: view.bounds.height / 2 - 100 / 2, width: 100, height: 100)
            hudView.tag = 100
            view.addSubview(hudView)
        } else {
            if let hudView = view.viewWithTag(100) {
                hudView.removeFromSuperview()
            }
        }
    }
    
    func setupGradients() {
        let gradientColors = [teal.cgColor, darkTeal.cgColor]
        let gradientDirection: [NSNumber] = [0.0, 1.0]
        
        let stationTitleGradient = CAGradientLayer()
        stationTitleGradient.frame = stationTitleBar.bounds
        stationTitleGradient.colors = gradientColors
        stationTitleGradient.locations = gradientDirection
        stationTitleBar.layer.addSublayer(stationTitleGradient)
        stationName.layer.zPosition = 1
        stationMoreLessButton.layer.zPosition = 1
        stationCloseButton.layer.zPosition = 1
        
        
        let summaryTitleGradient = CAGradientLayer()
        summaryTitleGradient.frame = summaryTitleBar.bounds
        summaryTitleGradient.colors = gradientColors
        summaryTitleGradient.locations = gradientDirection
        summaryTitleBar.layer.addSublayer(summaryTitleGradient)
        summaryLabel.layer.zPosition = 1
        toggleSummaryButton.layer.zPosition = 1
    }
    
    func initLabels() {
        
//        stationDefaultButton.layer.borderWidth = 1
//        stationDefaultButton.layer.borderColor = teal.cgColor
        
        let cornerRadius: CGFloat = 4
        let shadowRadius: CGFloat = 1
        let shadowOffset = CGSize(width: 1, height: 1)
        let shadowColor = UIColor.black.cgColor
        let shadowOpacity: Float = 1
        stationCloseButton.setBackgroundImage(CustomMapButton.cross(), for: UIControlState())
        stationCloseButton.layer.cornerRadius = cornerRadius
        stationCloseButton.layer.shadowRadius = shadowRadius
        stationCloseButton.layer.shadowOffset = shadowOffset
        stationCloseButton.layer.shadowColor = shadowColor
        stationCloseButton.layer.shadowOpacity = shadowOpacity
        
        stationMoreLessButton.layer.cornerRadius = cornerRadius
        stationMoreLessButton.layer.shadowRadius = shadowRadius
        stationMoreLessButton.layer.shadowOffset = shadowOffset
        stationMoreLessButton.layer.shadowColor = shadowColor
        stationMoreLessButton.layer.shadowOpacity = shadowOpacity
        
        toggleSummaryButton.layer.cornerRadius = cornerRadius
        toggleSummaryButton.layer.shadowRadius = shadowRadius
        toggleSummaryButton.layer.shadowOffset = shadowOffset
        toggleSummaryButton.layer.shadowColor = shadowColor
        toggleSummaryButton.layer.shadowOpacity = shadowOpacity
        
        if !isBarOpen {
            stationView.isHidden = true
            stationView.alpha = 0.0
        }
        if !isSummaryOpen { 
            summaryView.isHidden = true
            stationMoreLessToggleButton(isSummaryOpen as AnyObject)
        }
        
        let cornerSize: CGFloat = 6.0
        temperatureContainer.layer.cornerRadius = cornerSize
        humidityContainer.layer.cornerRadius = cornerSize
        pressureContainer.layer.cornerRadius = cornerSize
        windContainer.layer.cornerRadius = cornerSize
        rainContainer.layer.cornerRadius = cornerSize
        dewPointContainer.layer.cornerRadius = cornerSize
        uvContainer.layer.cornerRadius = cornerSize
        insolationContainer.layer.cornerRadius = cornerSize
        distanceContainer.layer.cornerRadius = cornerSize
        timeContainer.layer.cornerRadius = cornerSize
        
        tempArrow.image = CustomMapMarker.createArrow(isUp: summaryModeHighest, color1: orange, color2: orange)
        humidityArrow.image = CustomMapMarker.createArrow(isUp: summaryModeHighest, color1: purple, color2: purple)
        pressureArrow.image = CustomMapMarker.createArrow(isUp: summaryModeHighest, color1: pink, color2: pink)
        windArrow.image = CustomMapMarker.createArrow(isUp: summaryModeHighest, color1: yellow, color2: yellow)
        rainArrow.image = CustomMapMarker.createArrow(isUp: summaryModeHighest, color1: blue, color2: blue)
    }
    
    
    @IBAction func setDefaultButton(_ sender: AnyObject) {
        do {
            let realm = try! Realm()
            try realm.write {
                self.settings[0].useNearestStation = false
                self.settings[0].station = self.selectedObservation.stationName
                self.settings[0].stationTag = self.selectedObservation.stationTag
                self.settings[0].latitude = self.selectedObservation.latitude
                self.settings[0].longitude = self.selectedObservation.longitude
            }
        } catch {
            print("Error updating! " + "\(error)")
        }
        stationDefaultButton.isEnabled = false
    }
   
    
    @IBAction func stationMoreLessToggleButton(_ sender: AnyObject) {
        stationMoreLessButton.setBackgroundImage(moreStationInfo ? CustomMapButton.dash() : CustomMapButton.more(), for: UIControlState())
        
        func moreInfo() {
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10.0, options: [.curveEaseOut], animations: {
                self.uvIndexInsolationStackView.isHidden = false
                self.uvIndexInsolationStackView.alpha = 1.0
                self.distanceRainStackView.isHidden = false
                self.distanceRainStackView.alpha = 1.0
                self.timeContainer.isHidden = false
                self.timeContainer.alpha = 1.0
                if UIScreen.main.bounds.size.height == 480 {
                    self.windPressureStackView.isHidden = false
                    self.windPressureStackView.alpha = 1.0
                }
                self.stationStackView.layoutIfNeeded()
            }, completion: nil)
        }
        func lessInfo() {
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10.0, options: [.curveEaseOut], animations: {
                self.timeContainer.isHidden = true
                self.timeContainer.alpha = 0.0
                self.distanceRainStackView.isHidden = true
                self.distanceRainStackView.alpha = 0.0
                self.uvIndexInsolationStackView.isHidden = true
                self.uvIndexInsolationStackView.alpha = 0.0
                if UIScreen.main.bounds.size.height == 480 { // iPhone 4S
                    self.windPressureStackView.isHidden = true
                    self.windPressureStackView.alpha = 0.0
                }
                self.stationStackView.layoutIfNeeded()
            }, completion: nil)
        }
        if moreStationInfo {
            if isSummaryOpen {
                toggleSummaryBar(moreStationInfo as AnyObject)
            }
            moreInfo()
        } else {
            lessInfo()
        }
        moreStationInfo = !moreStationInfo
    }
    
    @IBAction func toggleStationBar(_ sender: AnyObject) {
        
        func toggleView() {
            isBarOpen = !isBarOpen
            stationView.isHidden = isBarOpen ? false : true
            
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10.0, options: [.curveEaseOut], animations: {
                self.stationView.alpha = self.isBarOpen ? 1.0 : 0.0
                self.view.layoutIfNeeded()
                }, completion: nil)
        }
        if sender as! NSObject == clusterManager {
            if isBarOpen {
                //Animate fields but not title.
                UIView.animate(withDuration: 0.5, delay: 0, options: [.transitionFlipFromRight], animations: {
                    self.updateInfoBarLabels(observation: self.selectedObservation)
                    }, completion: nil)
            } else {
                toggleView()
                updateInfoBarLabels(observation: selectedObservation)
                if isSummaryOpen {
                    toggleSummaryBar(clusterManager)
                }
            }
        } else if sender as! NSObject == stationCloseButton {
            toggleView()
        } else if sender as! NSObject == mapView {
            if isBarOpen {
                toggleView()
            } else {
                return
            }
        }
    }
    
    
    @IBAction func toggleSummaryBar(_ sender: AnyObject) {
        
        func toggleSummaryView() {
            isSummaryOpen = !isSummaryOpen
            toggleSummaryButton.setBackgroundImage(isSummaryOpen ? CustomMapButton.dash() : CustomMapButton.plus(), for: UIControlState())
            if summaryView.isHidden {
                summaryView.isHidden = false
            }
            
            UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10.0, options: [.curveEaseOut], animations: {
                self.summaryListStackView.isHidden = self.isSummaryOpen ? false : true
                self.summaryView.layoutIfNeeded()
            }, completion: nil)
            self.summaryListStackView.alpha = self.isSummaryOpen ? 1.0 : 0.0 // Fixes views hanging around towards end of animation bounce.
        }
        if sender as! NSObject == toggleSummaryButton {
            if !moreStationInfo {
                stationMoreLessToggleButton(toggleSummaryButton)
            }
            toggleSummaryView()
        } else if sender as! NSObject == clusterManager {
            toggleSummaryView()
        } else {
            summaryView.isHidden = false
            toggleSummaryView()
        }
    
    }
    
    func updateInfoBarLabels(observation: WeatherObservation) {
        
        if !stationDefaultButton.isEnabled {
            stationDefaultButton.isEnabled = true
        } else if settings[0].station == selectedObservation.stationName {
            stationDefaultButton.isEnabled = false
        }
        
        if UIScreen.main.bounds.size.height == 568 || UIScreen.main.bounds.size.height == 480 { // iPhone 5/4S
            let fontSize: CGFloat = 8.0
            let font = UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightThin)
            temperature.font = font
            temperatureLabel.font = font
            tempHigh.font = font
            tempHiLabel.font = font
            tempLow.font = font
            tempLowLabel.font = font
            humidity.font = font
            humidityLabel.font = font
            pressure.font = font
            pressureLabel.font = font
            pressureTrend.font = font
            pressureTrendLabel.font = font
            windSpeed.font = font
            windLabel.font = font
            windMax.font = font
            windMaxLabel.font = font
            heading.font = font
            headingLabel.font = font
            rain.font = font
            rainLabel.font = font
            dewPoint.font = font
            dewpointLabel.font = font
            rainRate.font = font
            rainRateLabel.font = font
            evapotranspiration.font = font
            evapotranspirationLabel.font = font
            uvIndex.font = font
            uvindexLabel.font = font
            insolation.font = font
            insolationLabel.font = font
            insolationPredicted.font = font
            insolationPredictedLabel.font = font
            elevation.font = font
            elevationLabel.font = font
            distance.font = font
            distanceLabel.font = font
            time.font = font
            timeLabel.font = font
            
            stationDefaultButton.titleLabel?.font = UIFont.systemFont(ofSize: 10, weight: UIFontWeightThin)
            setStationHeight.constant = 24
            
            let height: CGFloat = 10
            temperatureHeight.constant = 13
            humidityHeight.constant = height
            windHeight.constant = height
            windMaxHeight.constant = height
            headingHeight.constant = height
            insolationHeight.constant = height
            insolationPredictedHeight.constant = height
            rainHeight.constant = height
            rateHeight.constant = height
            evapotranspirationHeight.constant = height
            timeHeight.constant = height
            
            
        }
        
        stationName.text = observation.stationName
        temperature.text = observation.temperature + "°"
        tempHigh.text = observation.tempHi + "°"
        tempLow.text = observation.tempLo + "°"
        humidity.text = observation.humidity + "%"
        pressure.text = observation.pressure + " hPa"
        pressureTrend.text = observation.pressureTrend.localizedCapitalized
        windSpeed.text = observation.windSpeed + " km/h"
        windMax.text = observation.windSpeedMax + " km/h"
        heading.text = observation.windHeading
        rain.text = observation.rain + " mm"
        dewPoint.text = observation.dewPoint + "°"
        rainRate.text = observation.rainRate + " mm/h"
        evapotranspiration.text = observation.evapotranspiration + " mm"
        uvIndex.text = observation.uvIndex
        insolation.text = observation.insolation + " W/㎡"
        insolationPredicted.text = observation.insolationPredicted + " W/㎡"
        elevation.text = observation.elevation + " m"
        time.text = observation.observationTime
        if let location = currentLocation {
            distance.text = distanceBetweenLocations(currentLocation: location, targetLocation: observation.position)
        } else {
            distance.text = "--"
        }
    }

    func loadSettings() {
        settings = DataModel.load(.settings) as! Results<Settings>
        allStations = DataModel.load(.stations) as! Results<WeatherStation>
    }
    
    func centerMap(_ location: CLLocation, zoomLevel: Float) {
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: zoomLevel, bearing: 0, viewingAngle: 0)
    }
    
    //MARK: ZoomToLocation
    
    func zoomToLocation(location: CLLocationCoordinate2D, zoomLevel: Float) {
        let position = GMSCameraPosition(target: location, zoom: zoomLevel, bearing: 0.0, viewingAngle: 0.0)
        mapView.animate(to: position)
    }
    
    @IBAction func highestLowestSelection(_ sender: Any) {
        if summaryHiLo.selectedSegmentIndex == 0 {
            summaryModeHighest = true
        } else if summaryHiLo.selectedSegmentIndex == 1 {
            summaryModeHighest = false
        }
        tempArrow.image = CustomMapMarker.createArrow(isUp: summaryModeHighest, color1: orange, color2: orange)
        humidityArrow.image = CustomMapMarker.createArrow(isUp: summaryModeHighest, color1: purple, color2: purple)
        pressureArrow.image = CustomMapMarker.createArrow(isUp: summaryModeHighest, color1: pink, color2: pink)
        windArrow.image = CustomMapMarker.createArrow(isUp: summaryModeHighest, color1: yellow, color2: yellow)
        rainArrow.image = CustomMapMarker.createArrow(isUp: summaryModeHighest, color1: blue, color2: blue)
        
        temperatureList?.removeAll()
        humidityList?.removeAll()
        pressureList?.removeAll()
        windList?.removeAll()
        rainList?.removeAll()
        updateSummaryLabels()
    }
    
    enum ButtonCategory {
        case Temperature
        case Humidity
        case Pressure
        case Wind
        case Rain
    }
    
    func updateMapWithArrowMarker(buttonCategory: ButtonCategory) {
        
        if !clearButtonEnabled {
            addClearButton()
            clearButtonEnabled = true
        }
        
        let buttonTitle: String
        let markerColor: CGColor
        let observationList: [WeatherObservation]
        
        switch buttonCategory {
            case .Temperature:
                buttonTitle = summaryTemperatureStation.title(for: UIControlState())!
                markerColor = orange
                observationList = temperatureList ?? [WeatherObservation]()
            case .Humidity:
                buttonTitle = summaryHumidityStation.title(for: UIControlState())!
                markerColor = purple
                observationList = humidityList ?? [WeatherObservation]()
            case .Pressure:
                buttonTitle = summaryPressureStation.title(for: UIControlState())!
                markerColor = pink
                observationList = pressureList ?? [WeatherObservation]()
            case .Wind:
                buttonTitle = summaryWindStation.title(for: UIControlState())!
                markerColor = yellow
                observationList = windList ?? [WeatherObservation]()
            case .Rain:
                buttonTitle = summaryRainStation.title(for: UIControlState())!
                markerColor = blue
                observationList = rainList ?? [WeatherObservation]()
        }
        if observationList.count > 1 {
            let path = GMSMutablePath()
            for observation in observationList {
                createArrowMarker(isUp: summaryModeHighest, color1: markerColor, color2: markerColor, position: observation.position, category: buttonCategory)
                path.add(observation.position)
            }
            let bounds = GMSCoordinateBounds(path: path)
            let update = GMSCameraUpdate.fit(bounds, withPadding: 35.0)
            mapView.animate(with: update)
        } else {
            let station = allObservations.filter {$0.stationName == buttonTitle}
            if let station = station.first {
                if station.stationName.characters.count > 1 {
                    zoomToLocation(location: station.position, zoomLevel: 14.5)
                    createArrowMarker(isUp: summaryModeHighest, color1: markerColor, color2: markerColor, position: station.position, category: buttonCategory)
                }
            }
        }
    }
    
    @IBAction func temperatureButton(_ sender: Any) {
        updateMapWithArrowMarker(buttonCategory: .Temperature)
    }
    
    @IBAction func humidityButton(_ sender: Any) {
        updateMapWithArrowMarker(buttonCategory: .Humidity)
    }
    
    @IBAction func pressureButton(_ sender: Any) {
        updateMapWithArrowMarker(buttonCategory: .Pressure)
    }
    
    @IBAction func windButton(_ sender: Any) {
        updateMapWithArrowMarker(buttonCategory: .Wind)
    }
    
    @IBAction func rainButton(_ sender: Any) {
        updateMapWithArrowMarker(buttonCategory: .Rain)
    }
    
    func createArrowMarker(isUp: Bool, color1: CGColor, color2: CGColor, position: CLLocationCoordinate2D, category: ButtonCategory) {
        let duplicateLocation = arrowMarkers.filter {$0.position.latitude == position.latitude && $0.position.longitude == position.longitude}
        let duplicateCategory = arrowMarkers.filter {$0.userData as! ButtonCategory == category }
        
        if duplicateLocation.last?.position.latitude != position.latitude { // Create marker if no arrow at location.
            arrowMarkers.append(GMSMarker(position: position))
            arrowMarkers.last?.icon = CustomMapMarker.createArrow(isUp: isUp, color1: color1, color2: color2)
            arrowMarkers.last?.tracksViewChanges = false
            arrowMarkers.last?.appearAnimation = kGMSMarkerAnimationPop
            arrowMarkers.last?.groundAnchor = CGPoint(x: 0.5, y: 0.0)
            arrowMarkers.last?.userData = category
            arrowMarkers.last?.map = mapView
        } else if duplicateLocation.last?.position.latitude == position.latitude && duplicateCategory.count == 0{ // Already arrow at location and different color marker
            // Get original arrow marker
            let i = arrowMarkers.index(where: {$0.position.latitude == position.latitude})
            // Move original arrow groundAnchor
            if let i = i {
                arrowMarkers[i].groundAnchor = CGPoint(x: 0.9, y: 0.0)
            }
            arrowMarkers.append(GMSMarker(position: position))
            arrowMarkers.last?.icon = CustomMapMarker.createArrow(isUp: isUp, color1: color1, color2: color2)
            arrowMarkers.last?.tracksViewChanges = false
            arrowMarkers.last?.appearAnimation = kGMSMarkerAnimationPop
            arrowMarkers.last?.groundAnchor = CGPoint(x: 0.1, y: 0.0)
            arrowMarkers.last?.userData = category
            arrowMarkers.last?.map = mapView
        }
    }
   
    func updateSummaryLabels() {
        
        var observations = allObservations.filter {$0.temperature != "--"}
        observations = observations.filter {$0.humidity != "--"}
        observations = observations.filter {$0.pressure != "--"}
        observations = observations.filter {$0.windSpeed != "--"}
        observations = observations.filter {$0.rain != "--"}
        
        let temps = observations.map {($0.temperature as NSString).floatValue}
        let maxTemp = observations.filter {($0.temperature as NSString).floatValue == temps.max()}
        let minTemp = observations.filter {($0.temperature as NSString).floatValue == temps.min()}
        
        let humiditys = observations.map {($0.humidity as NSString).integerValue}
        let maxHumidity = observations.filter {($0.humidity as NSString).integerValue == humiditys.max()}
        let minHumidity = observations.filter {($0.humidity as NSString).integerValue == humiditys.min()}
        
        let pressures = observations.map {($0.pressure as NSString).integerValue}
        let maxPressure = observations.filter {($0.pressure  as NSString).integerValue == pressures.max()}
        let minPressure = observations.filter {($0.pressure  as NSString).integerValue == pressures.min()}
        
        let winds = observations.map {($0.windSpeed as NSString).integerValue}.filter {$0 != 0}
        let maxWind = observations.filter {($0.windSpeed as NSString).integerValue == winds.max()}
        let minWind = observations.filter {($0.windSpeed as NSString).integerValue == winds.min()}
        
        let rains = observations.map {($0.rain as NSString).floatValue}.filter {$0 != 0.00}
        let maxRain = observations.filter {($0.rain as NSString).floatValue == rains.max()}
        let minRain = observations.filter {($0.rain as NSString).floatValue == rains.min()}
        
        if summaryModeHighest { // Highest
            if maxTemp.count > 1 {
                summaryTemperatureStation.setTitle("\(maxTemp.count)" + " Stations", for: UIControlState())
                temperatureList = maxTemp
            } else if maxTemp.count == 1 {
                summaryTemperatureStation.setTitle(maxTemp.first?.stationName, for: UIControlState())
            }
            if let temp = temps.max() {
                summaryTemperature.text = String(temp) + "°"
            }
            
            if maxHumidity.count > 1 {
                summaryHumidityStation.setTitle("\(maxHumidity.count)" + " Stations", for: UIControlState())
                humidityList = maxHumidity
            } else if maxHumidity.count == 1 {
                summaryHumidityStation.setTitle(maxHumidity.first?.stationName, for: UIControlState())
            }
            if let hum = humiditys.max() {
                summaryHumidity.text = String(hum) + "%"
            }
            
            if maxPressure.count > 1 {
                summaryPressureStation.setTitle("\(maxPressure.count)" + " Stations", for: UIControlState())
                pressureList = maxPressure
            } else if maxPressure.count == 1 {
                summaryPressureStation.setTitle(maxPressure.first?.stationName, for: UIControlState())
            }
            if let press = pressures.max() {
                summaryPressure.text = String(press) + " hPa"
            }
            
            if maxWind.count > 1 {
                summaryWindStation.setTitle("\(maxWind.count)" + " Stations", for: UIControlState())
                windList = maxWind
            } else if maxWind.count == 1 {
                summaryWindStation.setTitle(maxWind.first?.stationName, for: UIControlState())
            }
            if let wind = winds.max() {
            summaryWind.text = String(wind) + " km/h"
            }
            
            if maxRain.count > 1 {
                summaryRainStation.setTitle("\(maxRain.count)" + " Stations", for: UIControlState())
                rainList = maxRain
            } else if maxRain.count == 1 {
                summaryRainStation.setTitle(maxRain.first?.stationName, for: UIControlState())
            }
            if let rain = rains.max() {
                summaryRain.text = String(rain) + " mm"
            }
        } else if !summaryModeHighest { // Lowest
            if minTemp.count > 1 {
                summaryTemperatureStation.setTitle("\(minTemp.count)" + " Stations", for: UIControlState())
                temperatureList = minTemp
            } else if minTemp.count == 1 {
                summaryTemperatureStation.setTitle(minTemp.first?.stationName, for: UIControlState())
            }
            if let temp = temps.min() {
                summaryTemperature.text = String(temp) + "°"
            }
    
            if minHumidity.count > 1 {
                summaryHumidityStation.setTitle("\(minHumidity.count)" + " Stations", for: UIControlState())
                humidityList = minHumidity
            } else if minHumidity.count == 1 {
               summaryHumidityStation.setTitle(minHumidity.first?.stationName, for: UIControlState())
            }
            if let hum = humiditys.min() {
                summaryHumidity.text = String(hum) + "%"
            }
            
            if minPressure.count > 1 {
                summaryPressureStation.setTitle("\(minPressure.count)" + " Stations", for: UIControlState())
                pressureList = minPressure
            } else if minPressure.count == 1 {
                summaryPressureStation.setTitle(minPressure.first?.stationName, for: UIControlState())
            }
            if let press = pressures.min() {
                summaryPressure.text = String(press) + " hPa"
            }
            
            if minWind.count > 1 {
                summaryWindStation.setTitle("\(minWind.count)" + " Stations", for: UIControlState())
                windList = minWind
            } else if minWind.count == 1 {
                summaryWindStation.setTitle(minWind.first?.stationName, for: UIControlState())
            }
            if let wind = winds.min() {
                summaryWind.text = String(wind) + " km/h"
            }
            
            if minRain.count > 1 {
                summaryRainStation.setTitle("\(minRain.count)" + " Stations", for: UIControlState())
                rainList = minRain
            } else if minRain.count == 1 {
                summaryRainStation.setTitle(minRain.first?.stationName, for: UIControlState())
            }
            if let rain = rains.min() {
                summaryRain.text = String(rain) + " mm"
            }
        }
    }
    
    func processAllObservationsAndDisplayPins() {
        clusterManager.add(allObservations)
        clusterManager.cluster()
    }
    
    //MARK: RequestFromServerDelegate Methods.
    
    func requestDidReceiveParsedData(_ requestType: RequestDataType, data: NSObject) {
        //Clear old observations
        allObservations.removeAll()
        allObservations = data as! [WeatherObservation]
        DispatchQueue.main.async {
            self.showActivityView(option: false)
            self.updateSummaryLabels()
            self.processAllObservationsAndDisplayPins()
            if !self.isSummaryOpen {
                self.toggleSummaryBar(self)
            }
            
        }
    }
    
    func requestDidFailWithError(_ requestType: RequestDataType, stationTag: String?, error: NSError) {
        DispatchQueue.main.async {
            self.showActivityView(option: false)
            let alert = Alerts.createAlert("Network Error", message: "There was a problem with the network. " + error.localizedDescription)
            self.present(alert, animated: true, completion: nil)
        }
    }

}

extension MapViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    //MARK: CLLocationManagerDelegate
    
    func checkLocationAuthorizationStatus() {
        locationManager.delegate = self
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.isMyLocationEnabled = true
            locationManager.requestLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentLocation = location
            zoomToLocation(location: location.coordinate, zoomLevel: 15)
            locationManager.delegate = nil
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("\(error)")
        let alert = Alerts.createAlert("Location Error", message: "There was a problem finding your location. " + error.localizedDescription)
        self.present(alert, animated: true, completion: nil)
    }
 
    //MARK: GMSMapViewDelegate
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        checkLocationAuthorizationStatus()
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        //Close info bar
        toggleStationBar(mapView)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.animate(toZoom: mapView.camera.zoom + 1)
        return false
    }
}

extension MapViewController: GMUClusterIconGenerator, GMUClusterManagerDelegate {
    
    //MARK:GMUClusterIconGenerator
    
    //Cluster Icons
    func icon(forSize size: UInt) -> UIImage! {
        return CustomMapMarker.createMarker("\(size)", humidity: "", isCluster: true)
    }
    
    //MARK: GMUClusterManagerDelegate
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) {
        mapView.animate(toLocation: cluster.position)
        mapView.animate(toZoom: mapView.camera.zoom + 1)
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) {
        if let station =  allObservations.first(where: {$0.stationId == clusterItem.clusterId}) {
            selectedObservation = station
            mapView.animate(toLocation: station.position)
            toggleStationBar(clusterManager)
        }
    }
}



//
//  WeatherViewController.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-07-17.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit
import RealmSwift
import Crashlytics

class WeatherViewController: UIViewController, RequestFromServerDelegate {

    @IBOutlet weak var titleBar: UIView!
    @IBOutlet weak var observationTime: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var tempHi: UILabel!
    @IBOutlet weak var tempSeparator: UILabel!
    @IBOutlet weak var tempLow: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var windHeading: UILabel!
    @IBOutlet weak var maxWindSpeed: UILabel!
    @IBOutlet weak var evapotranspiration: UILabel!
    @IBOutlet weak var insolationPredicted: UILabel!
    @IBOutlet weak var pressureTrend: UILabel!
    @IBOutlet weak var rainRate: UILabel!
    @IBOutlet weak var dewPoint: UILabel!
    @IBOutlet weak var DPLabel: UILabel!
    @IBOutlet weak var insolation: UILabel!
    @IBOutlet weak var uvIndex: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var stationName: UILabel!
    @IBOutlet weak var elevation: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var evapoLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var maxSpeedLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var uvIndexLabel: UILabel!
    @IBOutlet weak var insolationLabel: UILabel!
    @IBOutlet weak var insolationPredLabel: UILabel!
    @IBOutlet weak var elevationLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var humidityScaleView: ScaleView!
    @IBOutlet weak var pressureScaleView: ScaleView!
    @IBOutlet weak var rainScaleView: ScaleView!
    @IBOutlet weak var windGaugeView: WindGauge!
    @IBOutlet weak var thermometerView: TemperatureView!
    
    
    @IBOutlet weak var temperatureContainer: UIView!
    @IBOutlet weak var humidityContainer: UIView!
    @IBOutlet weak var pressureContainer: UIView!
    @IBOutlet weak var windContainer: UIView!
    @IBOutlet weak var rainContainer: UIView!
    @IBOutlet weak var uvIndexContainer: UIView!
    @IBOutlet weak var insolationContainer: UIView!
    @IBOutlet weak var distanceContainer: UIView!
    
    @IBOutlet weak var windTitle: UILabel!
    @IBOutlet weak var humidityTitle: UILabel!
    @IBOutlet weak var pressureTitle: UILabel!
    @IBOutlet weak var rainTitle: UILabel!
    
    
    var station = WeatherObservation()
    var nearestStation = WeatherStation()
    var stations = [WeatherStation]()
    
    var settings: Results<Settings>!
    var allStations: Results<WeatherStation>!
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var distanceToStation = ""
    var chosenStation = ""
    var useLocation = false
    var inactiveStationNames = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.movedToBackground), name: .UIApplicationWillResignActive, object: nil)
        adjustForScreenSize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if dataNeedsUpdating(observationTime: station.observationTime) {
            initLabels()
        }
        setupTitleGradient()
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateScenario()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func movedToBackground() {
        if self.isViewLoaded && self.view.window != nil {
            NotificationCenter.default.addObserver(self, selector: #selector(self.becameActive), name: .UIApplicationDidBecomeActive, object: nil)
        }
        loadData()
        Answers.logCustomEvent(withName: "User Settings", customAttributes: ["Station Name": settings[0].station, "Use Location?": settings[0].useNearestStation ? "Yes" : "No"])
    }
    
    func becameActive() {
        updateScenario()
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidBecomeActive, object: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    func numberSquared(number: Int) -> Int {
        
        return number * number
    }
    
    //MARK: RequestFromServerDelegate methods.
    
    func requestDidReceiveParsedData(_ requestType: RequestDataType, data: NSObject) {
        if requestType == .observation {
            let observation = data as! WeatherObservation
            station = observation
            DispatchQueue.main.async {
                if self.settings[0].useNearestStation && observation.temperature == "--" {
                    let alert = UIAlertController(title: "Observation Data Missing", message: "This station is currently missing observation data. Would you like to use the next nearest station?", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: {[unowned self] action in self.findUserLocation()})
                    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    alert.addAction(action)
                    alert.addAction(cancel)
                    self.present(alert, animated: true, completion: nil)
                    self.inactiveStationNames.append(observation.stationName)
                }
                self.showActivityView(option: false)
                self.updateLabels()
                do {
                    let realm = try! Realm()
                    try realm.write {
                        self.settings[0].station = self.station.stationName
                        self.settings[0].stationTag = self.station.stationTag
                        self.settings[0].latitude = self.station.latitude
                        self.settings[0].longitude = self.station.longitude
                    }
                } catch {
                    print("Error updating! " + "\(error)")
                }
                self.chosenStation = self.settings[0].station
            }
        } else if requestType == .allStations {
            DispatchQueue.main.async {
                self.showActivityView(option: false)
            }
            stations = data as! [WeatherStation]
            stations = stations.sorted(by: ({ $0.name < $1.name }))
            DataModel.save(.stations, data: stations as NSObject)
            //Get location permission
            findUserLocation()
        }
    }
    
    func requestDidFailWithError(_ requestType: RequestDataType, stationTag: String?, error: NSError) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Network Error", message: "There was a problem with the network. " + error.localizedDescription, preferredStyle: .alert)
            let action = UIAlertAction(title: "Retry", style: .default, handler: {[unowned self] action in
                self.requestWithDelay(requestType: requestType, stationTag: stationTag)})
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(action)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
            self.showActivityView(option: false)
        }
    }
    
    func requestWithDelay(requestType: RequestDataType, stationTag: String?) {
        let dispatchTime = DispatchTime.now() + DispatchTimeInterval.seconds(3)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            if dataNeedsUpdating(observationTime: self.station.observationTime) {
                self.showActivityView(option: true)
                let req = RequestFromServer(requestType: requestType, stationTag: stationTag)
                req.delegate = self
                req.request()
            }
        })
    }
    
    //Mark: Load data and get latest observation.
    
    func loadData() {
        settings = DataModel.load(.settings) as! Results<Settings>
        allStations = DataModel.load(.stations) as! Results<WeatherStation>
    }
    
    func updateAllStations() {
        showActivityView(option: true)
        let req = RequestFromServer(requestType: .allStations, stationTag: nil)
        req.delegate = self
        req.request()
    }
    
    func updateObservation(checkTime: Bool) {
        if checkTime {
            if dataNeedsUpdating(observationTime: station.observationTime) {
                showActivityView(option: true)
                let req = RequestFromServer(requestType: .observation, stationTag: settings[0].stationTag)
                req.delegate = self
                req.request()
            }
        } else {
            showActivityView(option: true)
            let req = RequestFromServer(requestType: .observation, stationTag: settings[0].stationTag)
            req.delegate = self
            req.request()
        }
    }
    
    func updateScenario() {
        if allStations.count == 0 {
            updateAllStations()
        } else if settings[0].useNearestStation {
            if settings[0].station != chosenStation {
                findUserLocation()
                useLocation = true
            } else if !useLocation {
                findUserLocation()
                useLocation = true
            } else {
                if dataNeedsUpdating(observationTime: station.observationTime) {
                    findUserLocation()
                }
            }
        } else if !settings[0].useNearestStation {
            useLocation = false
            if settings[0].station != chosenStation {
                updateObservation(checkTime: false)
                chosenStation = settings[0].station
            } else {
                updateObservation(checkTime: true)
            }
        }
    }
    
    func showActivityView(option: Bool) {
        if option {
            let hudView = ActivityView()
            hudView.frame = CGRect(x: view.bounds.width / 2 - 100 / 2, y: view.bounds.height / 2 - 100 / 2, width: 100, height: 100)
            hudView.tag = 100
            view.addSubview(hudView)
        } else {
            if let hudView = view.viewWithTag(100) {
                hudView.removeFromSuperview()
            }
        }
    }
    
    func setupTitleGradient() {
        let titleGradient = CAGradientLayer()
        titleGradient.frame = titleBar.frame
        titleGradient.colors = [teal.cgColor, darkTeal.cgColor]
        //titleGradient.locations = [0.0, 1.0]
        titleBar.layer.addSublayer(titleGradient)
        stationName.layer.zPosition = 1
    }
    
    func initLabels() {

        let opacitySetting: Float = 0.2
        
        temperatureContainer.layer.opacity = opacitySetting
        humidityContainer.layer.opacity = opacitySetting
        pressureContainer.layer.opacity = opacitySetting
        windContainer.layer.opacity = opacitySetting
        rainContainer.layer.opacity = opacitySetting
        uvIndexContainer.layer.opacity = opacitySetting
        insolationContainer.layer.opacity = opacitySetting
        distanceContainer.layer.opacity = opacitySetting

        stationName.text = ""
        observationTime.text = ""
        temperature.text = ""
        tempHi.text = ""
        
        tempLow.text = ""
        humidityScaleView.labelValue = ""
        distance.text = ""
        windHeading.text = ""
        maxWindSpeed.text = ""
        windSpeed.text = ""
        evapotranspiration.text = ""
        insolationPredicted.text = ""
        pressureTrend.text = ""
        rainRate.text = ""
        dewPoint.text = ""
        pressureScaleView.labelValue = ""
        rainScaleView.labelValue = ""
        insolation.text = ""
        uvIndex.text = ""
        elevation.text = ""
        
        DPLabel.layer.opacity = 0.0
        tempSeparator.layer.opacity = 0.0
        rateLabel.layer.opacity = 0.0
        evapoLabel.layer.opacity = 0.0
        windSpeedLabel.layer.opacity = 0.0
        maxSpeedLabel.layer.opacity = 0.0
        headingLabel.layer.opacity = 0.0
        uvIndexLabel.layer.opacity = 0.0
        insolationLabel.layer.opacity = 0.0
        insolationPredLabel.layer.opacity = 0.0
        elevationLabel.layer.opacity = 0.0
        distanceLabel.layer.opacity = 0.0
        timeLabel.layer.opacity = 0.0
        
        windTitle.layer.opacity = 0.0
        humidityTitle.layer.opacity = 0.0
        pressureTitle.layer.opacity = 0.0
        rainTitle.layer.opacity = 0.0
        
    }
    
    func updateLabels() {
        
        let animateOpacity = CABasicAnimation(keyPath: "opacity")
        animateOpacity.fromValue = 0.2
        animateOpacity.toValue = 1.0
        animateOpacity.duration = 0.5
        
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            
            self.DPLabel.layer.opacity = 1.0
            self.tempSeparator.layer.opacity = 1.0
            self.rateLabel.layer.opacity = 1.0
            self.evapoLabel.layer.opacity = 1.0
            self.windSpeedLabel.layer.opacity = 1.0
            self.maxSpeedLabel.layer.opacity = 1.0
            self.headingLabel.layer.opacity = 1.0
            self.uvIndexLabel.layer.opacity = 1.0
            self.insolationLabel.layer.opacity = 1.0
            self.insolationPredLabel.layer.opacity = 1.0
            self.elevationLabel.layer.opacity = 1.0
            self.distanceLabel.layer.opacity = 1.0
            self.timeLabel.layer.opacity = 1.0
            
            self.windTitle.layer.opacity = 1.0
            self.humidityTitle.layer.opacity = 1.0
            self.pressureTitle.layer.opacity = 1.0
            self.rainTitle.layer.opacity = 1.0
            
            self.stationName.text = self.station.stationName
            self.temperature.text = self.station.temperature + "°"
            self.tempHi.text = self.station.tempHi + "°"
            self.tempLow.text = self.station.tempLo + "°"
            self.dewPoint.text = self.station.dewPoint + "°"
            self.humidityScaleView.labelValue = self.station.humidity + "%"
            self.pressureTrend.text = self.station.pressureTrend.localizedCapitalized
            self.pressureScaleView.labelValue = self.station.pressure + " hPa"
            
            self.windHeading.text = self.station.windHeading
            self.maxWindSpeed.text = self.station.windSpeedMax + " km/h"
            self.evapotranspiration.text = self.station.evapotranspiration + " mm"
            self.insolationPredicted.text = self.station.insolationPredicted + " W/㎡"
            self.elevation.text = self.station.elevation + " m"
            self.rainRate.text = self.station.rainRate + " mm/h"
            self.insolation.text = self.station.insolation + " W/㎡"
            self.uvIndex.text = self.station.uvIndex
            //self.rain.text = self.station.rain + " mm"
            self.rainScaleView.labelValue = self.station.rain + " mm"
            self.windSpeed.text = self.station.windSpeed + " km/h"
            if let location = self.currentLocation {
                let fiveMinutesAgo = Date(timeIntervalSinceNow: -5 * 60)
                if location.timestamp >= fiveMinutesAgo {
                    self.distance.text = distanceBetweenLocations(currentLocation: location, targetLocation: self.station.position)
                } else {
                    self.distance.text = "--"
                }
            } else {
                self.distance.text = "--"
            }
            //self.rainScaleView.fillValue = CGFloat(Double(self.station.rain) ?? 0) * 2 // Scale calibration x2
            self.rainScaleView.curValue = CGFloat(Double(self.station.rain) ?? 0)
            self.humidityScaleView.curValue = CGFloat(Double(self.station.humidity) ?? 0)
            let pressure = CGFloat(Double(self.station.pressure) ?? 0)
            self.pressureScaleView.curValue = ((pressure - 850) * 250) / 250 //850hPa-1100hPa range 250, 0-250 points
            self.windGaugeView.degrees = CGFloat(Double(self.station.windSpeedDirection) ?? 0)
            let temperature = CGFloat(Double(self.station.temperature) ?? 0)
            self.thermometerView.fillValue = ((temperature - -30.0) * 105) / 80 // 0-105 points/-30-50 degrees.
            self.observationTime.text = self.station.observationTime
            
            self.stationName.layer.add(animateOpacity, forKey: nil)
            self.stationName.layer.opacity = 1.0
            
            self.temperatureContainer.layer.add(animateOpacity, forKey: nil)
            self.temperatureContainer.layer.opacity = 1.0
            
            self.humidityContainer.layer.add(animateOpacity, forKey: nil)
            self.humidityContainer.layer.opacity = 1.0
            
            self.pressureContainer.layer.add(animateOpacity, forKey: nil)
            self.pressureContainer.layer.opacity = 1.0
            
            self.windContainer.layer.add(animateOpacity, forKey: nil)
            self.windContainer.layer.opacity = 1.0
            
            self.rainContainer.layer.add(animateOpacity, forKey: nil)
            self.rainContainer.layer.opacity = 1.0
            
            self.uvIndexContainer.layer.add(animateOpacity, forKey: nil)
            self.uvIndexContainer.layer.opacity = 1.0
            
            self.insolationContainer.layer.add(animateOpacity, forKey: nil)
            self.insolationContainer.layer.opacity = 1.0
            
            self.distanceContainer.layer.add(animateOpacity, forKey: nil)
            self.distanceContainer.layer.opacity = 1.0
        })
        CATransaction.commit()
    
    }
    
    func adjustForScreenSize() {
        if UIScreen.main.bounds.size.height == 568 || UIScreen.main.bounds.size.height == 480 { // iPhone 5/4S
            let fontSize: CGFloat = 8.0
            let font = UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightThin)
            windSpeedLabel.font = font
            windSpeed.font = font
            maxSpeedLabel.font = font
            maxWindSpeed.font = font
            headingLabel.font = font
            windHeading.font = font
            
            uvIndexLabel.font = font
            uvIndex.font = font
            
            insolationLabel.font = font
            insolation.font = font
            
            insolationPredLabel.font = font
            insolationPredicted.font = font
            
            elevationLabel.font = font
            elevation.font = font
            
            distanceLabel.font = font
            distance.font = font
            
            timeLabel.font = font
            observationTime.font = font
        }
    }
    
}

// MARK: Location Manager


extension WeatherViewController: CLLocationManagerDelegate {
    
    func findUserLocation() {
        locationManager.delegate = self
        locationManager.distanceFilter = 100.0
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            locationManager.requestLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func findNearestStation(_ currentLocation: CLLocation) {
        var stationLocation = CLLocation()
        var distanceFromStation = CLLocationDistance()
        var previousDistance = 10000000.0 // 10,000 Km
        if allStations.count > 0 {
            for station in allStations {
                if station.name != inactiveStationNames.last {
                    stationLocation = CLLocation(latitude: station.latitude, longitude: station.longitude)
                    let distance = currentLocation.distance(from: stationLocation)
                    if distance <= previousDistance {
                        nearestStation = station
                        distanceFromStation = distance
                        previousDistance = distanceFromStation
                    }
                }
            }
            showActivityView(option: true)
            let req = RequestFromServer(requestType: .observation, stationTag: nearestStation.tag)
            req.delegate = self
            req.request()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            findUserLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.first
        if let location = locations.first {
            if abs(location.timestamp.timeIntervalSinceNow) < 60.0 {
                findNearestStation(location)
                locationManager.delegate = nil
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DispatchQueue.main.async {
            let alert = Alerts.createAlert("Location", message: "There was a problem finding your location. " + error.localizedDescription)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

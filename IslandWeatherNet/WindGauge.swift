//
//  Gauge.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-10-03.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit

//@IBDesignable
class WindGauge: UIView {

    override public var intrinsicContentSize: CGSize {
        get {
            return CGSize(width: circleSize + 10, height: circleSize + 10)
        }
    }
    
    var triangleWidth: CGFloat = 10.0
    var triangleHeight: CGFloat = 35.0
    var ballSize: CGFloat = 12.0
    var fontSize: CGFloat = 11
    var marginForText: CGFloat = 30
    let north = UILabel()
    let east = UILabel()
    let south = UILabel()
    let west = UILabel()
    var degrees: CGFloat = 0 {
        didSet {
            animate()
        }
    }
    
    let bgLayer = CAShapeLayer()
    @IBInspectable var bgColor: UIColor = UIColor.gray {
        didSet {
            configure()
        }
    }
    
    @IBInspectable var needleColor: UIColor = UIColor.gray {
        didSet {
            configure()
        }
    }
    
    let solidNeedle = CAShapeLayer()
    let hollowNeedle = CAShapeLayer()
    let ball = CAShapeLayer()
    
    @IBInspectable var circleWidth: CGFloat = 6.0 {
        didSet {
            configure()
        }
    }
    
    @IBInspectable var circleSize: CGFloat = 100.0 {
        didSet {
            configure()
        }
    }
    
    let deviceScaleFactor: CGFloat
    
    required init?(coder aDecoder: NSCoder) {
        if UIScreen.main.bounds.size.height == 480 {
            deviceScaleFactor = 0.55
            fontSize = 8
        } else if UIScreen.main.bounds.size.height == 568 {
            deviceScaleFactor = 0.9
        } else {
            deviceScaleFactor = 1
        }
        circleSize *= deviceScaleFactor
        triangleWidth *= deviceScaleFactor
        triangleHeight *= deviceScaleFactor
        ballSize *= deviceScaleFactor
        marginForText *= deviceScaleFactor
        super.init(frame: CGRect(x: 0, y: 0, width: circleSize + circleWidth, height: circleSize + circleWidth))
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        configure()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
        configure()
    }
    
    func setup() {
        // Setup background circle
        bgLayer.lineWidth = circleWidth
        bgLayer.fillColor = buttonBg.cgColor
        bgLayer.strokeEnd = 1
        layer.addSublayer(bgLayer)
        
        //Setup Text layers
        let gaugeFont = UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightThin)
        north.numberOfLines = 1
        north.adjustsFontSizeToFitWidth = true
        north.minimumScaleFactor = 0.5
        north.font = gaugeFont
        north.textColor = UIColor.white
        north.text = "N"
        north.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(north)
        
        east.numberOfLines = 1
        east.adjustsFontSizeToFitWidth = true
        east.minimumScaleFactor = 0.5
        east.font = gaugeFont
        east.textColor = UIColor.white
        east.text = "E"
        east.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(east)
        
        south.numberOfLines = 1
        south.adjustsFontSizeToFitWidth = true
        south.minimumScaleFactor = 0.5
        south.font = gaugeFont
        south.textColor = UIColor.white
        south.text = "S"
        south.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(south)
        
        west.numberOfLines = 1
        west.adjustsFontSizeToFitWidth = true
        west.minimumScaleFactor = 0.5
        west.font = gaugeFont
        west.textColor = UIColor.white
        west.text = "W"
        west.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(west)
        
        let northCenterX = north.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        let northCenterY = north.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -marginForText)
        
        let eastCenterX = east.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: marginForText)
        let eastCenterY = east.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        
        let southCenterX = south.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        let southCenterY = south.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: marginForText)
        
        let westCenterX = west.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: -marginForText)
        let westCenterY = west.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        
        NSLayoutConstraint.activate([northCenterX, northCenterY, eastCenterX, eastCenterY, southCenterX, southCenterY, westCenterX, westCenterY])
        
        // Setup solid pointer
        solidNeedle.lineWidth = 1.0
        solidNeedle.anchorPoint = CGPoint(x: 0.5, y: 1.0)
        solidNeedle.frame = CGRect(x: self.bounds.width / 2 - triangleWidth / 2, y: self.bounds.height / 2 - triangleHeight, width: triangleWidth, height: triangleHeight)
        let sPath = UIBezierPath()
        sPath.move(to: CGPoint(x: 0, y: triangleHeight))
        sPath.addLine(to: CGPoint(x: triangleWidth / 2, y: 0))
        sPath.addLine(to: CGPoint(x: triangleWidth, y: triangleHeight))
        solidNeedle.path = sPath.cgPath
        layer.addSublayer(solidNeedle)
        
        // Setup hollow pointer
        hollowNeedle.lineWidth = 1.0
        hollowNeedle.anchorPoint = CGPoint(x: 0.5, y: 0.0)
        hollowNeedle.frame = CGRect(x: self.bounds.width / 2 - triangleWidth / 2, y: self.bounds.height / 2, width: triangleWidth, height: triangleHeight)
        let hPath = UIBezierPath()
        hPath.move(to: CGPoint(x: 0, y: 0))
        hPath.addLine(to: CGPoint(x: triangleWidth / 2, y: triangleHeight))
        hPath.addLine(to: CGPoint(x: triangleWidth, y: 0))
        hollowNeedle.path = hPath.cgPath
        layer.addSublayer(hollowNeedle)
        
        // Setup needle center ball
        ball.fillColor = needleColor.cgColor
        ball.frame = CGRect(x: self.bounds.width / 2 - ballSize / 2, y: self.bounds.height / 2 - ballSize / 2, width: ballSize, height: ballSize)
        let ballPath = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: ballSize, height: ballSize))
        ball.path = ballPath.cgPath
        layer.addSublayer(ball)
        
        self.clipsToBounds = true
    }
    
    func configure() {
        bgLayer.strokeColor = bgColor.cgColor
        solidNeedle.fillColor = needleColor.cgColor
        solidNeedle.strokeColor = needleColor.cgColor
        hollowNeedle.strokeColor = needleColor.cgColor
        hollowNeedle.fillColor = UIColor.clear.cgColor
        ball.fillColor = needleColor.cgColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // Fix for messed up transform/animations when layoutSubviews is automatically called when there is a significant change to the view.
        let solidNeedleTransform: CATransform3D = solidNeedle.transform
        let hollowNeedleTransform: CATransform3D = hollowNeedle.transform
        solidNeedle.transform = CATransform3DIdentity
        hollowNeedle.transform = CATransform3DIdentity
        setup()
        configure()
        setupShapeLayer(shapeLayer: bgLayer)
        solidNeedle.transform = solidNeedleTransform
        hollowNeedle.transform = hollowNeedleTransform
    }
    
    private func setupShapeLayer(shapeLayer:CAShapeLayer) {
        shapeLayer.frame = self.bounds
        let path = UIBezierPath(ovalIn: CGRect(x: self.bounds.width / 2 - circleSize / 2, y: self.bounds.height / 2 - circleSize / 2, width: circleSize, height: circleSize))
        shapeLayer.path = path.cgPath
        
    }
    
    private func animate() {
        let positionTo = CGFloat(DegreesToRadians(value: degrees))
        let rotate = CABasicAnimation(keyPath: "transform.rotation.z")
        rotate.fromValue = DegreesToRadians(value: 0.0)
        rotate.toValue = positionTo 
        rotate.duration = 0.5
        solidNeedle.add(rotate, forKey: nil)
        hollowNeedle.add(rotate, forKey: nil)
        
        CATransaction.begin()
        solidNeedle.transform = CATransform3DMakeRotation(positionTo, 0.0, 0.0, 1.0)
        hollowNeedle.transform = CATransform3DMakeRotation(positionTo, 0.0, 0.0, 1.0)
        CATransaction.commit()
    }

}

//
//  DegreesToRadiens.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-09-26.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit
import Foundation

let π = CGFloat(M_PI)

func DegreesToRadians (value:CGFloat) -> CGFloat {
    return value * π / 180.0
}

func RadiansToDegrees (value:CGFloat) -> CGFloat {
    return value * 180.0 / π
}

func dataNeedsUpdating(observationTime: String?) -> Bool {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy/MM/dd, HH:mm"
    
    if let observationTime = observationTime {
        let formattedTime = dateFormatter.date(from: observationTime)
        if let difference = formattedTime?.timeIntervalSinceNow {
            let seconds = abs(difference as Double)
            if seconds > (5 * 60) {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    } else {
        return true
    }
}

func distanceBetweenLocations(currentLocation: CLLocation, targetLocation: CLLocationCoordinate2D) -> String {
    
    let distance = currentLocation.distance(from: CLLocation(latitude: targetLocation.latitude, longitude: targetLocation.longitude))
    let formattedDistance: String
    
    if #available(iOS 10.0, *) {
        let nearestStationMeasurement = Measurement(value: distance , unit: UnitLength.meters)
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .none
        let formatter = MeasurementFormatter()
        formatter.numberFormatter = numberFormatter
        formatter.unitOptions = [.naturalScale, .providedUnit]
        formattedDistance = formatter.string(from: nearestStationMeasurement)
    } else {
        formattedDistance = "\(Double((distance / 1000).rounded()))" + " km"
    }
    return formattedDistance
}

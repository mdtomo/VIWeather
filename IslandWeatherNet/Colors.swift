//
//  Colors.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-10-17.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import Foundation

let gray = UIColor.gray.cgColor
let darkGray = UIColor.darkGray.cgColor
let markerTxt = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.000)
let buttonBg = UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.000)
let buttonFg = UIColor.gray
let buttonFgSelected = UIColor.darkGray

let grayMainBg = UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.000)
let grayContainerBgLight = UIColor(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.000)
let grayContainerBg = UIColor(red: 50.0/255.0, green: 50.0/255.0, blue: 50.0/255.0, alpha: 1.000)
let grayTableSeparator = UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.000)

let purple = UIColor(red: 0.320, green: 0.356, blue: 0.800, alpha: 1.000).cgColor

let pink = UIColor(red: 0.800, green: 0.320, blue: 0.689, alpha: 1.000).cgColor

let lightTeal = UIColor(red: 115/255.0, green: 212/255.0, blue: 208/255.0, alpha: 1.0)
let teal = UIColor(red: 74/255.0, green: 186/255.0, blue: 182/255.0, alpha: 1.0)
let darkTeal = UIColor(red: 15/255.0, green: 145/255.0, blue: 140/255.0, alpha: 1.0)

let red = UIColor(red: 0.898, green: 0.349, blue: 0.204, alpha: 1.000).cgColor

let blue = UIColor(red: 0.357, green: 0.753, blue: 0.922, alpha: 1.000).cgColor
//let darkBlue = UIColor(red: 0.270, green: 0.333, blue: 0.498, alpha: 1.000)
//let lightBlue = UIColor(red: 0.310, green: 0.612, blue: 0.773, alpha: 1.000)

let green = UIColor(red: 0.608, green: 0.773, blue: 0.239, alpha: 1.000).cgColor

let yellow = UIColor(red: 0.992, green: 0.906, blue: 0.298, alpha: 1.000).cgColor

let orange = UIColor(red: 0.980, green: 0.475, blue: 0.129, alpha: 1.000).cgColor


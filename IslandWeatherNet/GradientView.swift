//
//  GradientView.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-12-27.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit

class GradientView: UIView {
    
//    override open class var layerClass: AnyClass {
//        get {
//            return CAGradientLayer.classForCoder()
//        }
//    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        let gradientLayer = self.layer as! CAGradientLayer
//        gradientLayer.colors = [grayContainerBgLight.cgColor, grayContainerBg.cgColor]
        self.layer.cornerRadius = 6.0
        self.layer.shadowRadius = 1.0
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 1.0
    }

}

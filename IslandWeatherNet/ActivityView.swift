//
//  ActivityView.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-10-26.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import Foundation
import UIKit

class ActivityView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [teal.cgColor, darkTeal.cgColor]
        gradient.locations = [0.0, 1.0]
        layer.addSublayer(gradient)
        gradient.cornerRadius = 6.0
        layer.shadowRadius = 1.0
        layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 1.0
        
        let activityView = UIActivityIndicatorView()
        activityView.tag = 101
        activityView.startAnimating()
        activityView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityView)
        
        //Constraints for activity spinner
        let activityViewCenterX = activityView.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        let activityViewCenterY = activityView.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        NSLayoutConstraint.activate([activityViewCenterX, activityViewCenterY])
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}

//
//  Alerts.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-08-28.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import Foundation
import UIKit

class Alerts: NSObject {
    
    static func createAlert(_ alertTitle: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: alertTitle,
                                      message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        return alert
    }
}

//
//  CustomTabBarController.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-08-24.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override var childViewControllerForStatusBarStyle : UIViewController? {
        return nil
    }
}

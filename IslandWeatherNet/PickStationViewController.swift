//
//  PickStationViewController.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-08-24.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import UIKit
import RealmSwift

class PickStationViewController: UITableViewController {

    var settings: Results<Settings>!
    var stations: Results<WeatherStation>!
    var filteredStations = [WeatherStation]()
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        configureSearchBar()
        definesPresentationContext = true
        extendedLayoutIncludesOpaqueBars = true
        tableView.tableHeaderView = searchController.searchBar
        tableView.backgroundColor = grayMainBg
        tableView.separatorColor = grayTableSeparator
        tableView.tableFooterView = UIView()
    }
    
    func loadData() {
        do {
            let realm = try Realm()
            stations = realm.objects(WeatherStation.self)
            settings = realm.objects(Settings.self)
        } catch {
            print("Something went wrong!" + "\(error)")
        }
    }
    
    func configureSearchBar() {
        searchController.loadViewIfNeeded()
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.barTintColor = buttonBg
        searchController.searchBar.tintColor = teal
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        do {
            let realm = try Realm()
            try realm.write {
                if searchController.isActive && searchController.searchBar.text != "" {
                    settings[0].station = filteredStations[(indexPath as NSIndexPath).row].name
                    settings[0].stationTag = filteredStations[(indexPath as NSIndexPath).row].tag
                    settings[0].useNearestStation = false
                } else {
                    settings[0].station = stations[(indexPath as NSIndexPath).row].name
                    settings[0].stationTag = stations[(indexPath as NSIndexPath).row].tag
                    settings[0].useNearestStation = false
                }
            }
        } catch {
            print("Something went wrong!" + "\(error)")
        }
        if let navController = navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredStations = stations.filter { station in
            return station.name.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredStations.count
        }
        return stations.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let station: WeatherStation
        if searchController.isActive && searchController.searchBar.text != "" {
            station = filteredStations[(indexPath as NSIndexPath).row]
        } else {
            station = stations[(indexPath as NSIndexPath).row]
        }
        cell.textLabel?.textColor = UIColor.lightGray
        cell.textLabel?.text = station.name
        return cell
    }
}

extension PickStationViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

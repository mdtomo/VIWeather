//
//  WeatherViewControllerSpec.swift
//  IslandWeatherNet
//
//  Created by Matthew Thomas on 2016-11-20.
//  Copyright © 2016 Matthew Thomas. All rights reserved.
//

import Foundation
import UIKit
import Quick
import Nimble
@testable import IslandWeatherNet


class WeatherViewControllerSpec: QuickSpec {
    
    override func spec() {
        
        let vc = WeatherViewController()
        
        describe("squareNumber") {
            it("Squares a given number") {
                expect(vc.numberSquared(number: 5)).to(equal(25))
            }
            
        }
        describe("dataNeedsUpdating") { // GlobalFunctions.swift
            
            it("Needs Updating > 5 mins old") {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy/MM/dd, HH:mm"
                let fiveMinsAgo = Date(timeInterval: -(5 * 60), since: Date())
                let fiveMinsAgoString = dateFormatter.string(from: fiveMinsAgo)
                
                expect(dataNeedsUpdating(observationTime: fiveMinsAgoString)).to(beTruthy())
            }
            it("Does Not Need Updating < 5 mins old") {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy/MM/dd, HH:mm"
                let dateTimeNow = dateFormatter.string(from: Date())
                
                expect(dataNeedsUpdating(observationTime: dateTimeNow)).notTo(beTruthy())
            }
        }
        describe("distanceBetweenLocations") { // GlobalFunctions.swift
            
            let currentLocation = CLLocation(latitude: 48.652097, longitude: -123.623098)
            
            it("Returns distance between Shawnigan Lake to Mill Bay (16 km)") {
                //let currentLocation = CLLocation(latitude: 48.652097, longitude: -123.623098)
                let brentwoodElementary = CLLocationCoordinate2D(latitude: 48.5745010, longitude: -123.4459991)
                // 15.65 Km
                expect(distanceBetweenLocations(currentLocation: currentLocation, targetLocation: brentwoodElementary)).to(equal("16 km"))
            }
            it("Returns distance between Shawnigan Lake and Mount Washington (174 km)") {
                let mtWashingtonResort = CLLocationCoordinate2D(latitude: 49.745499, longitude: -125.320007)
                // 173.43 Km
                expect(distanceBetweenLocations(currentLocation: currentLocation, targetLocation: mtWashingtonResort)).to(equal("174 km"))
            }
        }
    }
}






